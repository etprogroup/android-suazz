package com.etprogroup.suazz;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;


/**
 * Created by Bronco on 24-07-2017.
 */

public class Class_ListViewBooking extends BaseAdapter {
    private Context mContext;
    private final int ViewOrientation;
    private final int listViewLayout;
    private final JSONObject Bookings;

    public Class_ListViewBooking(Context context, JSONObject bookings){
        mContext = context;
        this.Bookings = bookings;
        this.listViewLayout = R.layout.listview_booking;
        this.ViewOrientation = LinearLayout.HORIZONTAL;
    }

    public Class_ListViewBooking(Context context, JSONObject bookings, int listview){
        mContext = context;
        this.Bookings = bookings;
        this.listViewLayout = listview;
        this.ViewOrientation = LinearLayout.HORIZONTAL;
    }

    @Override
    public int getCount() {
        return Bookings.length();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listView;
        ViewHolder viewHolder;
        String key = "";
        int iBookings = 0;

        Iterator<String> iter = Bookings.keys();
        while (iter.hasNext()) {
            String ikey = iter.next();
            if(position==iBookings){
                key = ikey;
                break;
            }
            iBookings++;
        }

        if (convertView == null) {
            //listView = new View(mContext);
            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(listViewLayout, null);
            RelativeLayout layoutView = (RelativeLayout) convertView.findViewById(R.id.layout);
            //layoutView.setOrientation(ViewOrientation);

            viewHolder.textID = (TextView) convertView.findViewById(R.id.textID);
            viewHolder.TextService = (TextView) convertView.findViewById(R.id.TextService);
            viewHolder.TextDate = (TextView) convertView.findViewById(R.id.TextDate);
            viewHolder.TextHour = (TextView) convertView.findViewById(R.id.TextHour);
            viewHolder.TextAddress = (TextView) convertView.findViewById(R.id.TextAddress);
            viewHolder.TextQuantity = (TextView) convertView.findViewById(R.id.TextQuantity);
            viewHolder.TextAmount = (TextView) convertView.findViewById(R.id.TextAmount);
            viewHolder.TextAutos = (TextView) convertView.findViewById(R.id.TextAutos);

            convertView.setTag(viewHolder);
            listView=convertView;

        }else{
            viewHolder = (ViewHolder) convertView.getTag();
            listView = convertView;
        }


        try {
            JSONObject Booking = Bookings.getJSONObject(key);
            String token = Booking.getString("token");
            String title = Booking.getString("title");
            String DateString = Booking.getString("date");
            String address = Booking.getString("address");
            String quantity = Booking.getString("quantity");
            String autos = Booking.getString("autos");
            String price = Booking.getString("price");
            String currency = Booking.getString("currency");

            long DateLong = Long.parseLong(DateString);//*1000
            Date Date = new Date(DateLong);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(Date);

            String format = "MM/dd/yy";
            SimpleDateFormat formatted = new SimpleDateFormat(format);
            String date = formatted.format(calendar.getTime());

            format = "HH:mm"; // :ss
            formatted = new SimpleDateFormat(format);
            String hour = formatted.format(calendar.getTime());

            viewHolder.textID.setText(token);
            viewHolder.TextAddress.setText(address);
            viewHolder.TextAutos.setText(autos);
            viewHolder.TextQuantity.setText(mContext.getString(R.string.ActivityProfileBookingQuantity)+" "+quantity);
            viewHolder.TextAmount.setText(mContext.getString(R.string.ActivityProfileBookingAmount)+" "+price+" "+currency);
            viewHolder.TextService.setText(mContext.getString(R.string.ActivityProfileBookingService)+" "+title+" "+price+" "+currency);
            viewHolder.TextDate.setText(mContext.getString(R.string.ActivityProfileBookingDate)+" "+date);
            viewHolder.TextHour.setText(mContext.getString(R.string.ActivityProfileBookingHour)+" "+hour);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return listView;
    }

    private static class ViewHolder {
        private TextView textID;
        private TextView TextAddress;
        private TextView TextQuantity;
        private TextView TextAutos;
        private TextView TextAmount;
        private TextView TextService;
        private TextView TextDate;
        private TextView TextHour;
    }




}