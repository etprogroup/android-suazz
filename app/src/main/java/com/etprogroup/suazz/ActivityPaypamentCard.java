package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;


/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityPaypamentCard extends ActivityRoot{
    public Activity mContext;

    //DATA USER DEFAULT
    Class_User _User;
    public String ServiceType;
    public Calendar ServiceDate;
    public int ServiceQuantity;
    public double ServiceTotal;

    //BUTTON
    public Button ButtonApply;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypamentcard);
        mContext = this;

        //USERDATA
        _User = ((App)getApplicationContext()).USERData();
        ServiceType = _User.GetDataServiceType();
        ServiceDate = _User.GetDataServiceDate();
        ServiceQuantity = _User.GetDataServiceQuantity();
        ServiceTotal = _User.GetDataServiceTotal();

        //HEADER SETPS
        new Class_HeaderSteps(this, "payments");

        //Button Data
        ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //Toast.makeText(ActivityServicesData.this, "cambiaria de layout", Toast.LENGTH_LONG).show();
                Intent Intent = new Intent(view.getContext(), ActivityAutos.class);
                startActivityForResult(Intent, 0);
            }

        });

    }

}
