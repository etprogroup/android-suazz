package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityProfileProvider extends ActivityRoot implements OnMapReadyCallback, LocationListener {
    public Activity mContext;
    public Class_User _User;
    private Class_DataServerFile DataServerFile;

    EditText EditImage;
    EditText EditTitle;
    EditText EditAgency;
    EditText EditTime;
    EditText EditPhone;
    EditText EditAddress;
    EditText EditDetail;
    TextView TextActive;
    TextView TextVisible;
    TextView TextRanking;
    TextView TextLocation;
    TextView TextLocationLat;
    TextView TextLocationLng;
    ImageView ProfileImageAgency;
    ImageView ProfileImage;
    Switch SwitchVisible;
    ImageView IconVisible;
    View FragmentMap;
    JSONObject DataProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profileprovider);
        mContext = this;

        //TOOLBAR
        new Class_Toolbar(mContext);

        //USER
        _User = ((App)getApplicationContext()).USERData();
        String image = _User.GetDataImage();

        ProfileImage = (ImageView) findViewById(R.id.ProfileImage);
        new Class_General().SetImageDefault(ProfileImage);
        String URL = new Class_General().CreateURL(mContext,image);
        new Class_ImageDownload(mContext, ProfileImage).execute(URL);

        EditImage = (EditText) findViewById(R.id.EditImage);
        EditTitle = (EditText) findViewById(R.id.EditTitle);
        EditAgency = (EditText) findViewById(R.id.EditAgency);
        EditPhone = (EditText) findViewById(R.id.EditPhone);
        EditAddress = (EditText) findViewById(R.id.EditAddress);
        EditTime = (EditText) findViewById(R.id.EditTime);
        EditDetail = (EditText) findViewById(R.id.EditDetail);
        TextRanking = (TextView) findViewById(R.id.TextRanking);
        SwitchVisible = (Switch) findViewById(R.id.SwitchVisible);
        TextVisible = (TextView) findViewById(R.id.TextProfileVisible);
        IconVisible = (ImageView) findViewById(R.id.IconVisible);

        //UPLOAD VISIBLE
        SwitchVisible.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UploadInfo();
            }
        });

       //UPLOAD IMAGE
        ProfileImageAgency = (ImageView) findViewById(R.id.ProfileImageAgency);
        new Class_General().SetImageDefault(ProfileImageAgency, R.drawable.services);
        ProfileImageAgency.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UploadImage();
            }
        });

        //SELECT MAP
        _Locations = ((App)getApplicationContext()).Locations();
        location = _Locations.location;
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();

        FragmentMap = (View) findViewById(R.id.map);
        TextLocationLat = (TextView) findViewById(R.id.TextLocationLat);
        TextLocationLat.setText(String.valueOf(latitude));
        TextLocationLng = (TextView) findViewById(R.id.TextLocationLng);
        TextLocationLng.setText(String.valueOf(longitude));
        //CreateMap();

        View FragmentMapOver = (View) findViewById(R.id.mapOver);
        LinearLayout LayoutLocation = (LinearLayout) findViewById(R.id.LayoutLocation);
        FragmentMapOver.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ActivityMapsLocation.class);
                startActivityForResult(intent, CAPTURE_MAPS_ACTIVITY);
            }
        });


        //Button Data
        Button ButtonApply;
        ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UploadInfo();
            }

        });




        //PROVIDER DATA
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "provider_data");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("provider", _User.GetDataToken());

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new Class_General(mContext).CreateMenssage("Update", output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new Class_General(mContext).CreateMenssage("success", message);
                        DataProvider = response.getJSONObject("data");
                        FillInfo();

                    }else if(result.equals("notfound")){
                        //new Class_General(mContext).CreateMenssage("notfound", message);


                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new Class_General(mContext).CreateMenssage("Error", message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        new Class_DataServer(mContext, data, response).execute("");
    }


    public void FillInfo(){
        try {
            String active = DataProvider.getString("active");
            String image = DataProvider.getString("image");
            String title = DataProvider.getString("title");
            String agency = DataProvider.getString("agency");
            String phone = DataProvider.getString("phone");
            String address = DataProvider.getString("address");
            String time = DataProvider.getString("time");
            String detail = DataProvider.getString("detail");
            String ranking = DataProvider.getString("ranking");
            String visible = DataProvider.getString("visible");
            latitude = Double.parseDouble(DataProvider.getString("latitude"));
            longitude = Double.parseDouble(DataProvider.getString("longitude"));

            if(active.equals("Y")){
                TextActive = (TextView) findViewById(R.id.TextActive);
                TextActive.setVisibility(View.GONE);
                TextActive.setTextColor(Color.GREEN);
                TextActive.setText(getString(R.string.ActivityProfileProviderActive));
            }

            if(image !=""){
                String URL = new Class_General().CreateURL(mContext,image);
                new Class_ImageDownload(mContext, ProfileImageAgency).execute(URL);
            }

            EditImage.setText(image);
            EditTitle.setText(title);
            EditAgency.setText(agency);
            EditPhone.setText(phone);
            EditAddress.setText(address);
            EditTime.setText(time);
            EditDetail.setText(detail);
            TextLocationLat.setText(String.valueOf(latitude));
            TextLocationLng.setText(String.valueOf(longitude));
            TextRanking.setText(ranking);
            CreateMap();

            if(visible.equals("Y")){
                SwitchVisible.setChecked(true);
                IconVisible.setImageResource(R.drawable.ic_eye);
                TextVisible.setText(R.string.ActivityProfileProviderVisibleProfile);
            }else{
                SwitchVisible.setChecked(false);
                IconVisible.setImageResource(R.drawable.ic_invisible);
                TextVisible.setText(R.string.ActivityProfileProviderHiddenProfile);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //UPLOAD INFO
    public void UploadInfo() {
        String visible = "N";
        IconVisible.setImageResource(R.drawable.ic_invisible);
        TextVisible.setText(R.string.ActivityProfileProviderHiddenProfile);
        if (SwitchVisible.isChecked()){
            //SwitchVisible.setChecked(true);
            IconVisible.setImageResource(R.drawable.ic_eye);
            TextVisible.setText(R.string.ActivityProfileProviderVisibleProfile);
            visible = "Y";
        }

        final JSONObject data = new JSONObject();
        try {
            data.put("action", "provider_update");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("provider", _User.GetDataToken());
            data.put("image", EditImage.getText().toString());
            data.put("title", EditTitle.getText().toString());
            data.put("agency", EditAgency.getText().toString());
            data.put("phone", EditPhone.getText().toString());
            data.put("address", EditAddress.getText().toString());
            data.put("time", EditTime.getText().toString());
            data.put("detail", EditDetail.getText().toString());
            data.put("latitude", TextLocationLat.getText().toString());
            data.put("longitude", TextLocationLng.getText().toString());
            data.put("visible", visible);

        } catch (JSONException e){
            e.printStackTrace();
        }


        Class_DataServer.AsyncResponse UserResponse = new Class_DataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new Class_General(mContext).CreateMenssage("Update", output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new Class_General(mContext).CreateMenssage("Message", message);
                        Intent intent = new Intent(mContext, ActivityProfile.class);
                        //startActivityForResult(intent, 0);

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new Class_General(mContext).CreateMenssage("Error", message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        new Class_DataServer(mContext, data, UserResponse).execute("");

    }



    //UPLOAD IMAGE
    private static final int CAPTURE_IMAGE_ACTIVITY = 1;
    private static final int CAPTURE_MAPS_ACTIVITY = 2;
    private Uri mImageCaptureUri;

    public void UploadImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CAPTURE_IMAGE_ACTIVITY);
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_MAPS_ACTIVITY) {
            if(resultCode == RESULT_OK) {
                latitude = Double.parseDouble(data.getStringExtra("latitude"));
                longitude = Double.parseDouble(data.getStringExtra("longitude"));
                TextLocationLat.setText(String.valueOf(latitude));
                TextLocationLng.setText(String.valueOf(longitude));
                CreateMap();
            }
        }

        if (requestCode == CAPTURE_IMAGE_ACTIVITY) {
            if (data == null) {
                return;
            }

            Uri URIimage = data.getData();
            String Image = new Class_General(mContext).getPathbyURI(mContext, URIimage);

            if(Image != null){
                File FileImage = new File(Image);


                if(FileImage.exists()){
                    UploadImages(Image, URIimage);

                }else{
                    new Class_General(mContext).CreateMenssage("Error", "Error Image Selected");

                }
            }

        }
    }



    public void UploadImages(final String path, final Uri URIimage){
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "image");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("path", "uploads/provider/");
            data.put("name_change", true);
            data.put("image_name", "file");
            data.put("image", path);

        } catch (JSONException e){
            e.printStackTrace();
        }



        final Class_DataServerFile.AsyncResponse FileResponse = new Class_DataServerFile.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new Class_General(mContext).CreateMenssage("output", output);
                //Log.d("output",output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("true")){
                        final String file = response.getString("file");
                        Class_ImageDownload.Cache.getInstance().getLru().remove(new Class_General().CreateURL(mContext,file));
                        ImageView ProfileImageAgency = (ImageView) findViewById(R.id.ProfileImageAgency);
                        ProfileImageAgency.setImageURI(URIimage);
                        EditImage.setText(file);


                    }else if(result.equals("false")){
                        new Class_General(mContext).CreateMenssage("Error", message);

                    }else if(result.equals("success")){

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        DataServerFile = new Class_DataServerFile(mContext, data, path, FileResponse);
        DataServerFile.execute("");
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(DataServerFile != null){
            DataServerFile.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //LOCATIONS
    private GoogleMap mMap;
    public Class_Locations _Locations;
    public Location location = null;
    static double latitude = 0;
    static double longitude = 0;
    public String AddressSelect = "";
    public Marker LocationMarker;

    public void CreateMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(19.0f);

        //CONTROLLER
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setScrollGesturesEnabled(false);
        uiSettings.setTiltGesturesEnabled(false);
        uiSettings.setRotateGesturesEnabled(false);
        uiSettings.setZoomGesturesEnabled(false);
        uiSettings.setMyLocationButtonEnabled(false);
        uiSettings.setZoomControlsEnabled(false);

        if(LocationMarker!=null){
            LocationMarker.remove();
        }


        LatLng location = new LatLng(latitude, longitude);
        MarkerOptions Marker = _Locations.CreateMarker(location,"Provider");
        LocationMarker = mMap.addMarker(Marker);
        //LocationMarker.showInfoWindow();

        LatLngBounds.Builder BuilderBounds = new LatLngBounds.Builder();
        BuilderBounds.include(Marker.getPosition());
        LatLngBounds bounds = BuilderBounds.build();
        //mMap.setLatLngBoundsForCameraTarget(bounds);

        final int width = getResources().getDisplayMetrics().widthPixels;
        final int height = getResources().getDisplayMetrics().heightPixels;
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.40);

        //CameraUpdate Camera = CameraUpdateFactory.newLatLng(location);
        CameraUpdate Camera = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(Camera);
        //mMap.moveCamera(Camera);
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", String.valueOf(location));
    }
}

