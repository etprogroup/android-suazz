package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

public class ActivityMaps extends ActivityRoot implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener, LocationListener {
    public Activity mContext;
    private GoogleMap mMap;
    public JSONObject Address = new JSONObject();
    public TextView TextViewAddress;

    //LOCATIONS
    public Class_Locations _Locations;
    public Location location = null;
    public double latitude = 0;
    public double longitude = 0;
    public String AddressSelect = "";
    public Marker LocationMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mContext = this;

        //HEADER SETPS
        new Class_HeaderSteps(this, "locations");

        //Button
        Button ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view){
                Intent PaypamentDataIntent = new Intent(view.getContext(), ActivityMapsProviders.class);
                startActivityForResult(PaypamentDataIntent, 0);
            }
        });

        //USERLOCATIONS
        _Locations = ((App)getApplicationContext()).Locations();
        location = _Locations.location;
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        UpdatePositionMap();
        

        //MAP AUTOCOMPLETE
        PlaceAutocompleteFragment placeAutocomplete = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.placeAutocomplete);

        EditText place_autocomplete_search_input = (EditText)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_search_input);
        place_autocomplete_search_input.setTextSize(15);
        place_autocomplete_search_input.setTextColor(Color.WHITE);
        place_autocomplete_search_input.setHintTextColor(Color.WHITE);

        ImageButton place_autocomplete_search_button = (ImageButton)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_search_button);
        place_autocomplete_search_button.setImageResource(R.drawable.ic_ubicacion);
        place_autocomplete_search_button.setColorFilter(Color.WHITE);

        ImageButton place_autocomplete_clear_button = (ImageButton)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_clear_button);
        place_autocomplete_clear_button.setColorFilter(Color.WHITE);

        ImageView place_autocomplete_powered_by_google = (ImageView)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_powered_by_google);
        //place_autocomplete_powered_by_google.setVisibility(View.GONE);

        placeAutocomplete.setHint(getResources().getString(R.string.ActivityMapSearch));
        if(AddressSelect!=""){
            placeAutocomplete.setText(AddressSelect);
        }

        try {
            placeAutocomplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    PlaceResult(place);
                }

                @Override
                public void onError(Status status) {
                    Log.d("Place error", "An error occurred: " + status);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void UpdatePositionMap(){
        _Locations.GetLocation(mContext, new Runnable() {
            public void run() {
                location = _Locations.location;
                latitude = _Locations.GetLatitude();
                longitude = _Locations.GetLongitude();
                CreateMap();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(15.0f);
        mMap.setMapStyle(_Locations.GetMapStyle());
        mMap.setInfoWindowAdapter(_Locations.CustomInfoWindowAdapter(mContext));
        mMap.setOnMarkerDragListener(this);

        //CONTROLLER
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setScrollGesturesEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);
        uiSettings.setTiltGesturesEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        //UiSettings.setMapToolbarEnabled(true);
        //UiSettings.setCompassEnabled(true);

        if(LocationMarker!=null){
            LocationMarker.remove();
        }

        LatLng location = new LatLng(latitude, longitude);
        MarkerOptions Marker = _Locations.CreateMarker(location,mContext.getString(R.string.ActivityMapLocation));
        LocationMarker = mMap.addMarker(Marker);
        //LocationMarker.showInfoWindow();

        LatLngBounds.Builder BuilderBounds = new LatLngBounds.Builder();
        BuilderBounds.include(Marker.getPosition());
        LatLngBounds bounds = BuilderBounds.build();
        //mMap.setLatLngBoundsForCameraTarget(bounds);

        final int width = getResources().getDisplayMetrics().widthPixels;
        final int height = getResources().getDisplayMetrics().heightPixels;
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.40);

        //CameraUpdate Camera = CameraUpdateFactory.newLatLng(location);
        CameraUpdate Camera = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(Camera);
        //mMap.moveCamera(Camera);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                _Locations.SetPlace(place);
                latitude = _Locations.GetLatitude();
                longitude = _Locations.GetLongitude();

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.d("Error", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                Log.d("Error", "Cancelado");
                // The user canceled the operation.
            }
        }
    }

    public void CreateMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void PlaceResult(Place place) {
        _Locations.SetPlace(place);
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        CreateMap();
    }


    public void onMarkerDrag(Marker marker){

    }

    public void onMarkerDragEnd (Marker marker){
        _Locations.SetMarker(marker);
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
    }

    public void onMarkerDragStart (Marker marker){

    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        _Locations.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", String.valueOf(location));

    }

}