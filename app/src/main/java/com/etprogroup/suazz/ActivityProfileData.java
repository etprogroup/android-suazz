package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityProfileData extends ActivityRoot {
    public Activity mContext;
    public Class_User _User;
    private Class_DataServerFile DataServerFile;

    EditText EditName;
    EditText EditLastName;
    EditText EditDate;
    EditText EditEmail;
    EditText EditPhone;
    EditText EditPass;
    EditText EditPassConfirm;
    ImageView ProfileImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profiledata);
        mContext = this;

        //TOOLBAR
        new Class_Toolbar(mContext);

        //USER
        _User = ((App)getApplicationContext()).USERData();
        String data = String.valueOf(_User.GetDataData());
        String user = _User.GetDataUser();
        String image = _User.GetDataImage();
        String fname = _User.GET_Data("fname", "value");
        String lname = _User.GET_Data("lname", "value");
        String phone = _User.GET_Data("phone", "value");
        String date = _User.GET_Data("date", "value");


        //UPLOAD IMAGE
        ProfileImage = (ImageView) findViewById(R.id.ProfileImage);
        new Class_General().SetImageDefault(ProfileImage);
        String URL = new Class_General().CreateURL(mContext,image);
        new Class_ImageDownload(mContext, ProfileImage).execute(URL);
        ProfileImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UploadImage();
            }
        });


        EditName = (EditText) findViewById(R.id.EditName);
        EditName.setText(fname);

        EditLastName = (EditText) findViewById(R.id.EditLastName);
        EditLastName.setText(lname);

        EditDate = (EditText) findViewById(R.id.EditDate);
        EditDate.setText(date);

        EditEmail = (EditText) findViewById(R.id.EditEmail);
        EditEmail.setText(user);

        EditPhone = (EditText) findViewById(R.id.EditPhone);
        EditPhone.setText(phone);

        EditPass = (EditText) findViewById(R.id.EditPass);
        EditPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        EditPassConfirm = (EditText) findViewById(R.id.EditPassConfirm);
        EditPassConfirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);


        //Button Data
        Button ButtonApply;
        ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UploadInfo();
            }

        });
    }


    //UPLOAD INFO
    public void UploadInfo() {

        final JSONObject data = new JSONObject();
        try {
            data.put("action", "user_update");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("fname", EditName.getText().toString());
            data.put("lname", EditLastName.getText().toString());
            data.put("phone", EditPhone.getText().toString());
            data.put("date", EditDate.getText().toString());
            data.put("pass", EditPass.getText().toString());
            data.put("passc", EditPassConfirm.getText().toString());

        } catch (JSONException e){
            e.printStackTrace();
        }


        Class_DataServer.AsyncResponse UserResponse = new Class_DataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new Class_General(mContext).CreateMenssage("Update", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        new Class_General(mContext).CreateMenssage("Update", message);
                        Intent intent = new Intent(mContext, ActivityProfile.class);
                        //startActivityForResult(intent, 0);

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new Class_General(mContext).CreateMenssage("Error", message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        new Class_DataServer(mContext, data, UserResponse).execute("");

    }



    //UPLOAD IMAGE
    private static final int CAPTURE_IMAGE_ACTIVITY = 1;
    private Uri mImageCaptureUri;

    public void UploadImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CAPTURE_IMAGE_ACTIVITY);
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY) {
            //TODO: action
            if (data == null) {
                //Display an error
                return;
            }

            Uri URIimage = data.getData();
            String Image = new Class_General(mContext).getPathbyURI(mContext, URIimage);

            if(Image != null){
                File FileImage = new File(Image);

                /*
                ArrayList<String>  ListImage = new Class_General(mContext).getRealPathFromURI(mContext, URIimage);
                String Image = ListImage.get(0);
                Log.d("ListImage", String.valueOf(ListImage));
                Log.d("Image",  String.valueOf(Image));
                */

                if(FileImage.exists()){
                    UploadImages(Image, URIimage);

                }else{
                    new Class_General(mContext).CreateMenssage("Error", "Error Image Selected");

                }
            }

        }
    }



    public void UploadImages(final String path, final Uri URIimage){
        final JSONObject data = new JSONObject();
            try {
            data.put("action", "image");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("path", "uploads/profile/");
            data.put("name_change", true);
            data.put("image_name", "file");
            data.put("image", path);

        } catch (JSONException e){
            e.printStackTrace();
        }



        final Class_DataServerFile.AsyncResponse FileResponse = new Class_DataServerFile.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new Class_General(mContext).CreateMenssage("output", output);
                //Log.d("output",output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("true")){
                        final String file = response.getString("file");
                        data.put("action", "user_image");
                        data.put("image", file);
                        _User.SetDataImage(file);

                        Class_DataServer.AsyncResponse UserResponse = new Class_DataServer.AsyncResponse() {
                            @Override
                            public void processFinish(String output) {
                                //new Class_General(mContext).CreateMenssage("output 2", output);
                                Class_ImageDownload.Cache.getInstance().getLru().remove(new Class_General().CreateURL(mContext,file));
                                ProfileImage.setImageURI(URIimage);

                            }
                        };
                        new Class_DataServer(mContext, data, UserResponse).execute("");

                    }else if(result.equals("false")){
                        new Class_General(mContext).CreateMenssage("Error", message);

                    }else if(result.equals("success")){

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        DataServerFile = new Class_DataServerFile(mContext, data, path, FileResponse);
        DataServerFile.execute("");
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(DataServerFile != null){
            DataServerFile.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}

