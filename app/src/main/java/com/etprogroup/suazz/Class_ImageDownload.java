package com.etprogroup.suazz;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import java.io.InputStream;

import static android.widget.ImageView.ScaleType.CENTER_CROP;
import static android.widget.ImageView.ScaleType.CENTER_INSIDE;

/**
 * Created by Bronco on 25-08-2017.
 */

public class Class_ImageDownload extends AsyncTask<String, Void, Bitmap> {
    Context mContext;
    String url = "";
    ImageView image;
    Boolean priority = false;
    ProgressDialog mProgressDialog;

    public Class_ImageDownload(Context context){
        mContext = context;
    }

    public Class_ImageDownload(Context context, ImageView img){
        mContext = context;
        image = img;
    }

    public Class_ImageDownload(Context context, ImageView img, Boolean Priority){
        priority = Priority;
        mContext = context;
        image = img;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(mContext, R.style.CustomStyleProgressDialog);
        //mProgressDialog.setTitle("Download Image");
        //mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }

    @Override
    protected Bitmap doInBackground(String... URL) {
        if (!image.equals("")) {
            String url = URL[0];
            Bitmap bitmap = null;
            InputStream input = null;

            bitmap = (Bitmap) Cache.getInstance().getLru().get(url);
            if (bitmap != null && !priority) {
                return bitmap;
            }

            try {
                input = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(input);

                if (bitmap != null) {
                    Cache.getInstance().getLru().put(url, bitmap);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result == null) {
            result = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_image);
            //image.setBackgroundResource(R.drawable.no_image);
            //mage.setScaleType(CENTER_INSIDE);
        }else{
            result.setHasAlpha(true);
            image.setImageBitmap(result);
            image.setScaleType(CENTER_CROP);
        }
        mProgressDialog.dismiss();
    }



    public static class Cache {
        private static Cache instance;
        private LruCache<Object, Object> lru;

        private Cache() {
            lru = new LruCache<Object, Object>(1024);
        }

        public static Cache getInstance() {
            if (instance == null) {
                instance = new Cache();
            }
            return instance;
        }

        public LruCache<Object, Object> getLru() {
            return lru;
        }
    }

}