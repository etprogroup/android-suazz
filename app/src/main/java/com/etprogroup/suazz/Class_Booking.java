package com.etprogroup.suazz;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Created by Carlos Gomez on 14-11-2017.
 */

class Class_Booking {
    public Activity mContext;
    public JSONObject Bookings;

    public ListView Listview;
    private Class_ListViewBooking adapterView;

    public Class_Booking(Activity context) {
        mContext = context;
    }


    public void  FillBooking(JSONObject data, final Runnable callback){

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse() {

            @Override
            public void processFinish(String output) {
                //new Class_General(mContext).CreateMenssage("Booking", output);
                //Log.d("Booking",output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new Class_General(mContext).CreateMenssage("success", message);
                        //Log.d("bookings",response.getString("bookings"));
                        Bookings = new JSONObject();

                        String Sbookings = response.getString("bookings");
                        Object JSONbookings = new JSONTokener(Sbookings).nextValue();

                        if (JSONbookings instanceof JSONObject){
                            Bookings = response.getJSONObject("bookings");

                        }else if(JSONbookings instanceof JSONArray){

                        }

                        ListBooking(callback);

                    }else if(result.equals("etoken")){
                        new Class_General(mContext).ActivityRedirect(ActivityServices.class);

                    }else{
                        new Class_General(mContext).CreateMenssage("Error", message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataServer(mContext, data, response).execute("");
    }



    private void ListBooking(Runnable callback){
        adapterView = new Class_ListViewBooking(mContext, Bookings);
        Listview=(ListView) mContext.findViewById(R.id.ListBooking);
        Listview.setAdapter(adapterView);
        RefreshListView();
        callback.run();
    }


    public void RefreshListView(){
        int Quantity = Bookings.length();

        if(Quantity>0){

            ListAdapter listAdapter = Listview.getAdapter();
            if(listAdapter == null) {
                return;
            }

            int Height = 0;
            for(int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, Listview);
                //istItem.measure(0, 0);
                listItem.measure(
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                Height += listItem.getMeasuredHeight();
            }

            Height += Listview.getDividerHeight()*(listAdapter.getCount()-1);
            ViewGroup.LayoutParams params = Listview.getLayoutParams();
            params.height = Height;
            Listview.setLayoutParams(params);
            //ListviewAutos.setExpand(true);

            adapterView.notifyDataSetChanged();

        }else{
            Listview.setVisibility(View.GONE);
            TextView TextBookingNotFound = (TextView) mContext.findViewById(R.id.TextBookingNotFound);
            TextBookingNotFound.setVisibility(View.VISIBLE);
        }
    }


    public void BookingResponse(JSONObject data, String booking) {
        Bookings.remove(booking);
        RefreshListView();

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse() {

            @Override
            public void processFinish(String output) {
                //new Class_General(mContext).CreateMenssage("Booking response", output);

                try {
                    JSONObject response = new JSONObject(output);
                    String title = response.getString("title");
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new Class_General(mContext).CreateMenssage("success", message);
                        //Log.d("providers",response.getString("providers"));

                        //Badges
                        new Class_DataStorage(mContext, "write", "badges", response.getString("badges")).execute("");


                    }else if(result.equals("etoken")){
                        new Class_General(mContext).ActivityRedirect(ActivityProfileBookingProviderPending.class);

                    }else{
                        new Class_General(mContext).CreateMenssage("Error", message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataServer(mContext, data, response).execute("");
    }



    public JSONObject RankingQuestions(String ID){
        JSONObject Questions = new JSONObject();
        try {
            JSONObject Booking = Bookings.getJSONObject(ID);
            JSONObject Ranking = Booking.getJSONObject("ranking");
            Questions = Ranking.getJSONObject("questions");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Questions;
    }
}
