package com.etprogroup.suazz;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Carlos Gomez on 18-11-2017.
 */

public class Service_Locations extends Service {
    private static final String TAG = "APPTESTGPS";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10;
    private static final int PERMISSIONS_ACCESS_LOCATION = 1;
    public Context mContext = null;

    public Class_Locations _Locations;
    public Location location;
    public double latitude = 0;
    public double longitude = 0;
    private String user = "";
    private String token = "";

    private class LocationListener implements android.location.LocationListener{
        Location mLastLocation;

        public LocationListener(String provider){
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location){
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            setLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras){
            Log.e(TAG, "onStatusChanged: " + provider);
        }

        @Override
        public void onProviderDisabled(String provider){
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider){
            Log.e(TAG, "onProviderEnabled: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0){
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        mContext = this;
        Log.e(TAG, "onCreate");
        _Locations = ((App)getApplicationContext()).Locations();
        initializeLocationManager();

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);

        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);

        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());

        }

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);

        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);

        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);
        Log.e(TAG, "onStart");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.e(TAG, "onDestroy");

        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);

                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(this.LOCATION_SERVICE);
        }
    }


    private void setLocation(Location location) {
        location = _Locations.GetLastKnownLocations(this);
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        StorageLocation();
    }


    private void StorageLocation() {
        // TODO: Implement this method to send token to your app server.
        Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                Log.e(TAG, "StorageLocation:"+output);

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("token")){
                        token = Callback.getString("token");
                    }

                    if(!Callback.isNull("user")){
                        user = Callback.getString("user");
                    }

                    if(!token.equals("") && !user.equals("")){
                        ServerLocation();
                    }

                }catch(JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataStorage(this, "read", "user", "", Callback).execute("");
    }

    public void ServerLocation(){

        JSONObject data = new JSONObject();
        try {
            data.put("action", "locations");
            data.put("user", user);
            data.put("token", token);
            data.put("latitude", String.valueOf(latitude));
            data.put("longitude", String.valueOf(longitude));

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                Log.e(TAG, "Servicelocations:"+output);
                //retunr server
            }
        };

        new Class_DataServer(this, data, response).execute("");
    }

}
