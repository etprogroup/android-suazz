package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityPaypamentData extends ActivityRoot {
    public Activity mContext;
    private String booking;
    private String sbookings;
    private JSONObject bookings =  new JSONObject();
    private Class_Services _Services;
    private Class_User _User;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypamentdata);
        mContext = this;

        //HEADER SETPS
        new Class_HeaderSteps(this, "payments");

        _User = ((App)getApplicationContext()).USERData();
        _Services = ((App)getApplicationContext()).Services();


        Intent data = getIntent();
        booking = data.getStringExtra("booking");
        sbookings = data.getStringExtra("bookings");

        if(booking!=null && sbookings!=null){
            if(!booking.equals("") && !sbookings.equals("")){
                try {
                    bookings = new JSONObject(sbookings);
                    FillData();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public void FillData(){
        try {
            JSONObject Booking = bookings.getJSONObject(booking);
            String token = Booking.getString("token");
            String title = Booking.getString("title");
            String service = Booking.getString("service");
            String DateString = Booking.getString("date");
            String address = Booking.getString("address");
            String quantity = Booking.getString("quantity");
            String autos = Booking.getString("autos");
            String price = Booking.getString("price");
            String currency = Booking.getString("currency");

            long DateLong = Long.parseLong(DateString);//*1000
            Date Date = new Date(DateLong);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(Date);

            String format = "MM/dd/yy";
            SimpleDateFormat formatted = new SimpleDateFormat(format);
            String date = formatted.format(calendar.getTime());

            format = "HH:mm"; // :ss
            formatted = new SimpleDateFormat(format);
            String hour = formatted.format(calendar.getTime());

            title = _Services.GET_Data(service, "title");
            String duration = _Services.GET_Data(service, "duration");
            price = _Services.GET_Data(service, "price");

            TextView Address = (TextView) findViewById(R.id.TextPaypamentAddress);
            Address.setText(address);

            TextView Autos = (TextView) findViewById(R.id.TextPaypamentAutos);
            Autos.setText(autos);

            TextView Quantity = (TextView) findViewById(R.id.TextPaypamentQuantity);
            Quantity.setText(String.valueOf(quantity));

            //SERVICES
            TextView services = (TextView) findViewById(R.id.TitlePaypamentService);
            services.setText(title);

            TextView prices = (TextView) findViewById(R.id.TextPaypamentServicePrice);
            prices.setText(mContext.getString(R.string.PaypalCurrencyIcon)+"  "+price);

            TextView durations = (TextView) findViewById(R.id.TextPaypamentServiceTime);
            durations.setText(duration);

            //DATE
            TextView dates = (TextView) findViewById(R.id.DataPaypamentDate);
            dates.setText(date);

            TextView Hour = (TextView) findViewById(R.id.DataPaypamentHour);
            Hour.setText(hour);

            //Button Data
            Button ButtonApply = (Button) findViewById(R.id.ButtonApply);
            ButtonApply.setEnabled(true);
            ButtonApply.setVisibility(View.VISIBLE);
            ButtonApply.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    //Toast.makeText(ActivityServicesData.this, "cambiaria de layout", Toast.LENGTH_LONG).show();
                    Intent Intent = new Intent(view.getContext(), ActivityPaypamentAmount.class);
                    Intent.putExtra("booking", booking);
                    Intent.putExtra("bookings", sbookings);
                    startActivityForResult(Intent, 0);
                }

            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void ClickViewService(View view) {
        Intent Intent = new Intent(view.getContext(), ActivityServices.class);
        //startActivityForResult(Intent, 0);
    }

    public void ClickViewServiceData(View view) {
        Intent Intent = new Intent(view.getContext(), ActivityServicesData.class);
        //startActivityForResult(Intent, 0);
    }

}
