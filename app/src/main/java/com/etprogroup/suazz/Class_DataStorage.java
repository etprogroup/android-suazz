package com.etprogroup.suazz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Bronco on 10-09-2017.
 */

public class Class_DataStorage extends AsyncTask<String, Void, String> {
    public Context mContext;
    public static final int CONNECTION_TIMEOUT = 15 * 1000;
    ProgressDialog mProgressDialog = null;
    public AsyncResponse delegate = null;
    public String data;
    public String action;
    public String option;
    public String fileName = "config.txt";

    public Class_DataStorage(Context context, String action, String option, String StringData) {
        this.mContext = context.getApplicationContext();
        this.option = option;
        this.action = action;
        this.data = StringData;
        this.delegate =  new AsyncResponse(){
            @Override
            public void processFinish(String output){
                //TODO: Action
            }
        };
    }

    public Class_DataStorage(Context context, String action, String option, String StringData, AsyncResponse delegate) {
        this.mContext = context.getApplicationContext();
        this.delegate = delegate;
        this.option = option;
        this.action = action;
        this.data = StringData;


        if(fileExists(fileName)){

        }else{
            //File file = new File(mContext.getFilesDir(), fileName);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(mContext != null && mContext instanceof Activity) {
            mProgressDialog = new ProgressDialog(mContext, R.style.CustomStyleProgressDialog);
            //mProgressDialog.setTitle("Conected");
            //mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }


    protected String doInBackground(String... params) {
        String DataFile = "";

        if(action.equals("write")){
            DataFile = writeToFile(option, data);

        }else if(action.equals("read")){
            DataFile = readFromFileOption(option);

        }else{
            DataFile = readFromFile();
        }

        return DataFile;
    }

    protected void onPostExecute(String result) {
        delegate.processFinish(result);

        if(mProgressDialog != null){
            mProgressDialog.dismiss();

        }
    }

    public interface AsyncResponse {
        void processFinish(String output);
    }


    private String readFromFile() {
        String data = "";

        if(fileExists(fileName)){
            //Log.d("storagelog-File", "file Exist");

            try {
                FileInputStream fis = mContext.openFileInput(fileName);
                InputStreamReader isr = new InputStreamReader(fis);
                BufferedReader bufferedReader = new BufferedReader(isr);
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                }

                data = String.valueOf(sb);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }else{
            Log.e("storagelog-File", "file no Exist");
        }


        if(data.equals("")){
            data="{}";
        }

        return data;
    }

    public boolean fileExists(String filename) {
        File file = mContext.getFileStreamPath(filename);
        return !(file == null || !file.exists());
    }

    private String readFromFileOption(final String option) {
        String DataFile =  readFromFile();

        try {
            JSONObject DataJSON = new JSONObject(DataFile);

            if(DataJSON.isNull(option)){
                DataFile = "{}";

            }else{
                DataFile = DataJSON.getString(option);
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return DataFile;
    }

    private String writeToFile(String option, String data) {
        String DataFile = readFromFile();

        try {
            JSONObject DataJSON = new JSONObject(DataFile);
            DataJSON.put(option, data);

            DataFile = DataJSON.toString();

            FileOutputStream openFile = mContext.openFileOutput(fileName, mContext.MODE_PRIVATE);
            openFile.write(DataFile.getBytes());
            openFile.close();


        }catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return DataFile;
    }


}
