package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityNotification extends ActivityRoot {
    public Activity mContext;
    public Class_User _User;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        mContext = this;

        //TOOLBAR
        new Class_Toolbar(mContext);

        _User = ((App)getApplicationContext()).USERData();

        //TITLE
        TextView TextNotifyTitle = (TextView) findViewById(R.id.TextNotifyTitle);
        TextNotifyTitle.setText(getString(R.string.ActivityNotifyTitle));

        //MESSAGE
        TextView TextNotifyMessage = (TextView) findViewById(R.id.TextNotifyMessage);
        TextNotifyMessage.setText(getString(R.string.ActivityNotifyMessage));

        //MESSAGE
        Button NotifyButton = (Button) findViewById(R.id.ButtonApply);
        NotifyButton.setText(getString(R.string.ActivityNotifyAction));
        NotifyButton.setVisibility(View.GONE);
        NotifyButton.setEnabled(false);

        //DATA
        Intent data = getIntent();
        String title = data.getStringExtra("title");
        String message = data.getStringExtra("message");
        final String action = data.getStringExtra("action");
        String actiontext = data.getStringExtra("actiontext");

        Bundle extra = data.getExtras();
        String etitle = extra.getString("title");
        String body = extra.getString("classname");
        String classname = extra.getString("classname");
        String option = extra.getString("option");

        //REDIRECT NOTIFY
        if(classname!=null){
            if(!classname.equals("")){
                Intent Intent = new Intent(mContext, ActivityMain.class);
                Intent.putExtra("classname", classname);
                if(option!=null) {
                    Intent.putExtra("option", option);
                }
                startActivity(Intent);
            }
        }


        if(title!=null){
            if(!title.equals("")){
                TextNotifyTitle.setText(title);
            }
        }else if(etitle!=null){
            if(!etitle.equals("")){
                TextNotifyTitle.setText(etitle);
            }
        }

        if(message!=null){
            if(!message.equals("")){
                TextNotifyMessage.setText(message);
            }

        }else if(body!=null){
            if(!body.equals("")){
                TextNotifyMessage.setText(body);
            }

        }


        if(action!=null){
            if(!action.equals("")){

                if(actiontext!=null){
                    if(!actiontext.equals("")){
                        NotifyButton.setText(actiontext);
                    }
                }

                NotifyButton.setEnabled(true);
                NotifyButton.setVisibility(View.VISIBLE);
                NotifyButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Class activity = ActivityMain.class;

                        try {
                            activity = Class.forName(action);

                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                        Intent Intent = new Intent(mContext, activity);
                        startActivity(Intent);
                    }
                });
            }
        }
    }

}
