package com.etprogroup.suazz;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Gomez on 07-11-2017.
 */

public class Service_FirebaseInstanceIDService extends FirebaseInstanceIdService {
    public Class_User _User;
    public Context mContext = null;
    public String refreshedToken;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    @Override
    public void onTokenRefresh() {
        refreshedToken = GetToken();
        Log.e("Refreshed token: ", "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    public String GetToken() {
        // Get updated InstanceID token.
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        return refreshedToken;
    }


    private void sendRegistrationToServer(final String device) {
        // TODO: Implement this method to send token to your app server.

        Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Log.d("output", output);

                try {
                    JSONObject Callback = new JSONObject(output);
                    String user = "";
                    String token = "";

                    if(!Callback.isNull("token")){
                        token = Callback.getString("token");
                    }

                    if(!Callback.isNull("user")){
                        user = Callback.getString("user");
                    }

                    if(token.equals("") || user.equals("")){
                        UserServer(user, token, device);
                    }

                }catch(JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataStorage(this, "read", "user", "", Callback).execute("");

    }

    public void UserServer(String user, String token, String device){

        JSONObject data = new JSONObject();
        try {
            data.put("action", "token");
            data.put("user", user);
            data.put("token", token);
            data.put("device", device);

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Log.d("output server", output);
                //retunr server
            }
        };

        new Class_DataServer(this, data, response).execute("");
    }
}
