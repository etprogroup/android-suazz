package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivitySigninConfirmed extends ActivityRoot {
    private Context mContext;
    private EditText userCode;
    private Class_User _User;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signinconfirmed);
        mContext = this;

        _User = ((App)getApplicationContext()).USERData();
        userCode = (EditText) findViewById(R.id.EditCode);

        //Button
        Button ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Validate();
            }

        });

    }


    public void Validate(){
        JSONObject data = new JSONObject();
        try {
            data.put("action", "validate");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("code", userCode.getText().toString());

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Toast.makeText(getApplicationContext(), output, Toast.LENGTH_SHORT).show();

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");
                    final String title = response.getString("title");

                    if(result.equals("success")){
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        _User.SetDataData(response.getString("data"));
                        //new Class_General(mContext).CreateMenssage(title, output);

                        JSONObject user = new JSONObject();
                        user.put("token", _User.GetDataToken());
                        user.put("user", _User.GetDataUser());
                        user.put("data", _User.GetDataData());

                        Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

                            @Override
                            public void processFinish(String output){
                                //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();
                                //new Class_General(mContext).CreateMenssage("SAVE", output);
                            }
                        };

                        new Class_DataStorage(mContext, "write", "user", user.toString(), Callback).execute("");

                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivityForResult(Intent, 0);

                    }else if(result.equals("error")){
                        new Class_General(mContext).CreateMenssage(title, message);
                        //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        new Class_DataServer(mContext, data, response).execute("");
    }

}
