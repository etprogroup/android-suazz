package com.etprogroup.suazz;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by Carlos Gomez on 07-11-2017.
 */

public class Service_FirebaseMessagingService extends FirebaseMessagingService {
    String TAGLog = "TESTFirebase";
    String body = null;
    String title = null;
    String action = null;
    String tag = null;
    String classname = null;
    String option = null;
    String messageCount = null;
    Map<String, String> data = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAGLog, "remoteMessage: " + String.valueOf(remoteMessage));
        Log.e(TAGLog, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAGLog, "Message data payload: " + remoteMessage.getData());
            data = remoteMessage.getData();
            classname = data.get("classname");
            messageCount = data.get("messageCount");
            option = data.get("option");

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAGLog, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            body = remoteMessage.getNotification().getBody();
            title = remoteMessage.getNotification().getTitle();
            action = remoteMessage.getNotification().getClickAction();
            tag = remoteMessage.getNotification().getTag();
        }

        sendNotification();

    }



    private void sendNotification() {
        Log.e(TAGLog, "sendNotification: ");

        int IntMessageCount = 0;
        if(messageCount!=null){
            if(messageCount!=""){
                IntMessageCount = Integer.parseInt(messageCount);
            }
        }

        Class ClassAction = ActivityMain.class;
        Intent resultIntent = new Intent(this, ClassAction);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if(classname!=null){
            if(classname!=""){
                resultIntent.putExtra("classname", classname);
            }
        }


        if(option!=null){
            if(option!=""){
                resultIntent.putExtra("option", option);
            }
        }

        if(tag!=null){
            if(tag!=""){
                resultIntent.putExtra("tag", tag);
            }
        }

        if(data!=null){
            resultIntent.putExtra("data", String.valueOf(data));
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0 ,
                resultIntent,
                PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.launcher)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setColor(Color.WHITE)
                        .setSound(defaultSoundUri)
                        .setNumber(IntMessageCount)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0 , notificationBuilder.build());

        /*
        //notification badges
        String id = getString(R.string.ActivityNotifyChannel);;
        CharSequence name = getString(R.string.ActivityNotifyChannelName);
        String description = getString(R.string.ActivityNotifyChannelDesc);
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);

        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        notificationManager.createNotificationChannel(mChannel);
        */
    }
}
