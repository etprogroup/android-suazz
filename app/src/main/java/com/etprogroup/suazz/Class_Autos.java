package com.etprogroup.suazz;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Bronco on 20-07-2017.
 */

public class Class_Autos extends Application {
    private JSONObject UserAutos = new JSONObject();
    private JSONObject UserAutosSelect = new JSONObject();
    private int UserAutosSelectQuantity = 0;
    private Context mContext;

    //DATA USER DEFAULT
    Class_User _User;

    public Class_Autos(Context context) {
        mContext = context;
        //CreateDataUserAutos();
    }

    public void CreateDataUserAutos(){
        for(int i=0 ; i<4; i++){
            AddDataUserAutos(String.valueOf(i), "Marca"+i, "Modelo"+i, "Placa"+i);
        }
    }

    public void AddDataUserAutos(String prefix, String brand, String modelo, String licenseplate){
        JSONObject auto = new JSONObject();
        try{
            auto.put("id", prefix);
            auto.put("brand", brand);
            auto.put("model", modelo);
            auto.put("licenseplate", licenseplate);
            AddJSONDataUserAutos(prefix, auto);

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public void AddJSONDataUserAutos(String prefix, JSONObject auto){
        String ID = GenerateUserAutosID(prefix);

        try{
            auto.put("id", ID);
            auto.put("selected", true);
            SetJSONDataUserAutos(ID,auto);

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public void SetJSONDataUserAutos(String ID, JSONObject auto){
        try{
            UserAutos.put(ID,auto);

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    public String GenerateUserAutosID(String prefix){
        Long time = System.currentTimeMillis();
        String ID = String.valueOf(prefix+time);

        /*
        Iterator<String> iter = UserAutos.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            if(key.equals(ID)){
                return GenerateUserAutosID((ID+1));
            }
        }
        */
        return ID;
    }



    public String GET_Data(String name, String type){
        String data = "";
        try {
            JSONObject auto = UserAutos.getJSONObject(name);
            data = auto.getString(type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public void SET_Data(String name, String data, String type){
        try {
            JSONObject auto = UserAutos.getJSONObject(name);
            auto.put(type, data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //BY POSITION
    public String GET_DataByPosition(int position, String type){
        String data = "";
        int ListKey = 0;
        Iterator<String> iter = UserAutos.keys();
        while(iter.hasNext()) {
            String key = iter.next();
            if (ListKey == position) {
                data = GET_Data(key, type);
                break;
            }
            ListKey++;
        }

        return data;
    }

    public void SET_DataByPosition(int position, String data, String type){
        int ListKey = 0;
        Iterator<String> iter = UserAutos.keys();
        while(iter.hasNext()) {
            String key = iter.next();
            if (ListKey == position) {
                SET_Data(key, data, type);
                break;
            }
            ListKey++;
        }
    }



    //BY ID
    public String GET_DataByID(String ID, String type){
        String data = "";
        int ListKey = 0;
        Iterator<String> iter = UserAutos.keys();
        while(iter.hasNext()) {
            String key = iter.next();
            if (key.equals(ID)) {
                data = GET_Data(key, type);
                break;
            }
            ListKey++;
        }

        return data;
    }

    public void SET_DataByID(String ID, String data, String type){
        int ListKey = 0;
        Iterator<String> iter = UserAutos.keys();
        while(iter.hasNext()) {
            String key = iter.next();
            if (key.equals(ID)) {
                SET_Data(key, data, type);
                break;
            }
            ListKey++;
        }
    }

    public JSONObject DELETE_DataByID(String ID){
        this.UserAutos.remove(ID);
        return UserAutos;
    }

    public String GetDetailUserAutos() {
        String data = "";
        int ListKey = 0;
        Iterator<String> iter = UserAutosSelect.keys();
        while(iter.hasNext()) {
            String key = iter.next();
            String prefix = "";
            if(ListKey > 0){
                prefix = ", ";
            }
            String Brand = GET_Data(key, "brand");
            String Model = GET_Data(key, "model");
            data = data+prefix+Brand+" "+Model;
            ListKey++;
        }

        return data;
    }

    public int GetUserAutosQuantity() {
        UserAutosSelectQuantity = UserAutosSelect.length();
        return UserAutosSelectQuantity;
    }



    public void SetDataUserAutos(JSONObject Autos){
        UserAutos = Autos;
    }

    public JSONObject GetDataUserAutos(){
        return UserAutos;
    }

    public void SetDataUserAutosSelect(JSONObject Autos){
        UserAutosSelect = Autos;
    }

    public JSONObject GetDataUserAutosSelect(){
        return UserAutosSelect;
    }



    public void SetDataServer_Autos(final Activity context, JSONObject Autos){
        _User = ((App)context.getApplicationContext()).USERData();
        JSONObject data = new JSONObject();
        try {
            data.put("action", "autos");
            data.put("option", "set");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("autos", Autos.toString());

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");

                    if(result.equals("success")){
                        String message = response.getString("message");
                        //new Class_General(context).CreateMenssage("Exito", output);
                        //Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

                    }else if(result.equals("error")){
                        String message = response.getString("message");
                        new Class_General(context).CreateMenssage("Error", message);
                        //Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        new Class_DataServer(context, data, response).execute("");
    }



    public JSONObject GetDataServer_Autos(final Activity context, final Runnable Callback){
        _User = ((App)context.getApplicationContext()).USERData();
        JSONObject data = new JSONObject();
        try {
            data.put("action", "autos");
            data.put("option", "get");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());


        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new Class_General(context).CreateMenssage("Exito", output);
                        //Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        UserAutos = new JSONObject(response.getString("autos"));
                        Callback.run();

                    }else if(result.equals("error")){
                        new Class_General(context).CreateMenssage("Error", message);
                        //Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataServer(context, data, response).execute("");
        return UserAutos;
    }



    public void SetDataStorage_Autos(final Context context, JSONObject Autos){
        Class_DataStorage.AsyncResponse response = new Class_DataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();
                //new Class_General(context).CreateMenssage("SAVE", output);
            }
        };

        new Class_DataStorage(context, "write", "autos", Autos.toString(), response).execute("");
    }



    public JSONObject GetDataStorage_Autos(final Context context, final Runnable Callback){
        Class_DataStorage.AsyncResponse response = new Class_DataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();

                try {
                    JSONObject response = new JSONObject(output);
                    //new Class_General(context).CreateMenssage("EGetDataStorage_Autosxito", output);
                    //Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    UserAutos = response;
                    Callback.run();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataStorage(context, "read", "autos", "", response).execute("");
        return UserAutos;
    }
}
