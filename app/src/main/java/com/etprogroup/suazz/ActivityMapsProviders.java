package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ActivityMapsProviders extends ActivityRoot implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener  {
    private Activity mContext;
    private JSONObject Providers = new JSONObject();
    private Map<MarkerOptions, String> markerIds = new HashMap<>();
    private GoogleMap mMap;
    private Class_User _User;
    private Class_Providers _Providers;

    //LOCATIONS
    private Class_Locations _Locations;
    private double latitude = 0;
    private double longitude = 0;
    private int distance;
    private Marker LocationMarker = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapsproviders);
        mContext = this;

        //HEADER SETPS
        new Class_HeaderSteps(this, "locations");

        _User = ((App) getApplicationContext()).USERData();
        _Providers = ((App) getApplicationContext()).Providers();
        _Locations = ((App) getApplicationContext()).Locations();
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        distance = _Locations.distance;

        //CONFIG
        Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("latitude")){
                    }

                    if(!Callback.isNull("longitude")){
                    }

                    if(!Callback.isNull("distance")){
                        _Locations.distance= Integer.parseInt(Callback.getString("distance"));
                    }

                    //PROVIDERS
                    ProviderSearch();

                }catch(JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        new Class_DataStorage(mContext, "read", "locations", "", Callback).execute("");


        final TextView TextViewDistance = (TextView) findViewById(R.id.TextDistance);
        SeekBar seekBarDistance = (SeekBar ) findViewById(R.id.distance);
        seekBarDistance.setProgress(distance);
        TextViewDistance.setText(String.valueOf(distance)+" "+getString(R.string.ActivityMapDistance));
        seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                TextViewDistance.setText(String.valueOf(distance)+" "+getString(R.string.ActivityMapDistance));
                distance = progresValue;
                progress = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                TextViewDistance.setText(String.valueOf(distance)+" "+getString(R.string.ActivityMapDistance));
                SetDistance(distance);
                UpdateStorage();
                ProviderSearch();
            }
        });

    }

    private void ProviderSearch(){
        final Runnable ProvidersCallback = new Runnable() {
            @Override
            public void run() {
                Providers=_Providers.Providers;
                if(Providers.length()>0){
                    CreateMap();

                }else{
                    new Class_General(mContext).CreateMenssage("Providers", String.valueOf(mContext.getText(R.string.MessageProvidersNotFound)));

                }
            }
        };

        _Providers.GetMarkerProvidersServer(mContext, _User, ProvidersCallback);
    }

    public void SetDistance(int distance){
        _Locations.distance = distance;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(15.0f);//19
        mMap.setMapStyle(_Locations.GetMapStyle());
        mMap.setInfoWindowAdapter(_Locations.CustomInfoWindowAdapter(mContext));


        if(LocationMarker!=null){
            LocationMarker.remove();
        }

        //User
        LatLng location = new LatLng(latitude, longitude);
        MarkerOptions Marker = _Locations.CreateMarker(location,mContext.getString(R.string.ActivityMapLocation));
        LocationMarker = mMap.addMarker(Marker);
        //LocationMarker.showInfoWindow();

        LatLngBounds.Builder BuilderBounds = new LatLngBounds.Builder();
        BuilderBounds.include(Marker.getPosition());

        //Providers
        Iterator<String> iter = Providers.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                JSONObject provider = Providers.getJSONObject(key);
                double Platitude = Double.parseDouble(provider.getString("latitude"));
                double Plongitude = Double.parseDouble(provider.getString("longitude"));
                String PTitle = provider.getString("title");

                location = new LatLng(Platitude, Plongitude);
                Marker = _Locations.CreateMarkerProviders(location, key, PTitle);

                markerIds.put(Marker, key);
                LocationMarker = mMap.addMarker(Marker);
                //LocationMarker.showInfoWindow();
                BuilderBounds.include(Marker.getPosition());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final int width = getResources().getDisplayMetrics().widthPixels;
        final int height = getResources().getDisplayMetrics().heightPixels;
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.40);

        LatLngBounds bounds = BuilderBounds.build();
        mMap.setLatLngBoundsForCameraTarget(bounds);
        //mMap.setOnInfoWindowClickListener(this);

        //CameraUpdate Camera = CameraUpdateFactory.newLatLng(location);
        CameraUpdate Camera = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        mMap.animateCamera(Camera);
        //mMap.moveCamera(Camera);
    }


    public void CreateMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        _Locations.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onInfoWindowClick(Marker marker) {
        onMarkerClick(marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.hideInfoWindow();
        //String id = markerIds.get(marker);
        String id = marker.getSnippet();
        //Log.d("onMarkerClick",String.valueOf(id));
        if(id!="" && id!=null){
            _Providers.ProviderSelect = id;
            Intent intent = new Intent(this, ActivityProvider.class);
            intent.putExtra("PROVIDER_SELECT",id);
            startActivity(intent);
        }
        return false;
    }

    private void UpdateStorage(){
        try {
            JSONObject user = new JSONObject();
            user.put("latitude", String.valueOf(_Locations.latitude));
            user.put("longitude", String.valueOf(_Locations.longitude));
            user.put("distance", String.valueOf(_Locations.distance));
            Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

                @Override
                public void processFinish(String output){
                    //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();
                    //new Class_General(mContext).CreateMenssage("SAVE", output);
                }
            };

            new Class_DataStorage(mContext, "write", "locations", user.toString(), Callback).execute("");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}