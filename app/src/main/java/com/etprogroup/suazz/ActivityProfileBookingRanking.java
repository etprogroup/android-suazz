package com.etprogroup.suazz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Carlos Gomez on 08-11-2017.
 */

public class ActivityProfileBookingRanking extends ActivityRoot {
    public Activity mContext;
    public Class_User _User;
    public Class_Booking _Booking;

    public ListView Listview;
    public View ListViewSelect;
    final int Style = R.style.CustomStyleAlertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookings);
        mContext = this;

        //TOOLBAR
        new Class_Toolbar(mContext);

        //TITLE
        TextView TextTitle = (TextView) findViewById(R.id.TextBooking);
        TextTitle.setText(getString(R.string.ActivityProfileBookingRanking));

        _Booking = new Class_Booking(mContext);
        _User = ((App)getApplicationContext()).USERData();
        _User.UserCheck(mContext, ActivityProfileBookingRanking.class);


        Bookings();
        final SwipeRefreshLayout SwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.SwipeRefreshLayout);
        SwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        SwipeRefreshLayout.setRefreshing(false);
                        Bookings();
                    }
                }
        );
    }


    private void Bookings(){
        Runnable Callback = new Runnable() {
            @Override
            public void run() {
                BookingAction();
            }
        };

        try {
            //BOOKING DATA
            final JSONObject data = new JSONObject();
            data.put("action", "bookings");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("type", "close_ranking");
            _Booking.FillBooking(data, Callback);

        } catch (JSONException e){
            e.printStackTrace();
        }
    }


    private void BookingAction() {
        Listview = _Booking.Listview;
        Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                ListViewSelect = parent.getChildAt(position);

                final TextView textViewID = (TextView) ListViewSelect.findViewById(R.id.textID);
                final String textID = (String) textViewID.getText();


                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, Style);
                builder.setTitle(getString(R.string.RankingTitle));
                builder.setMessage(getString(R.string.RankingMessage));

                //LAYOUT
                final JSONObject Questions = _Booking.RankingQuestions(textID);
                final LinearLayout BuilderLayout = new LinearLayout(mContext);
                BuilderLayout.setOrientation(LinearLayout.VERTICAL);
                BuilderLayout.setPadding(80,20,80,20);

                if(Questions.length()>0){
                    try {
                        Iterator<String> iter = Questions.keys();
                        while(iter.hasNext()) {
                            String key = iter.next();
                            String title = Questions.getString(key);

                            final LinearLayout Service = GridRanking(key, title);
                            BuilderLayout.addView(Service);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                builder.setView(BuilderLayout);

                builder.setPositiveButton(getString(R.string.RankingButtonPositive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        int ranking = 0;
                        int count = Questions.length();
                        JSONObject answer = new JSONObject();
                        if(count>0) {


                            try {
                                final int size = BuilderLayout.getChildCount();
                                for (int i = 0; i < size; i++) {
                                    if (BuilderLayout.getChildAt(i) instanceof LinearLayout) {

                                        ViewGroup GridRanking = (ViewGroup) BuilderLayout.getChildAt(i);

                                        TextView TextViewKey = (TextView) GridRanking.findViewById(R.id.RatingGridKey);
                                        String key = String.valueOf(TextViewKey.getText());

                                        EditText EditTextValue = (EditText) GridRanking.findViewById(R.id.RatingGridValue);
                                        String value = String.valueOf(EditTextValue.getText());

                                        ranking = ranking+Integer.parseInt(value);
                                        answer.put(key,value);
                                        Log.e("RankingValue","key: "+key+"  //  "+value);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        //Ranking
                        ranking = ranking/count;
                        BookingResponse(textID, ranking, answer);

                    }
                });

                builder.setNeutralButton(getString(R.string.RankingButtonNegative), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    private void BookingResponse(String booking, int ranking, JSONObject answer) {
        //BOOKING DATA
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "booking_ranking");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("booking", booking);
            data.put("ranking", String.valueOf(ranking));
            data.put("answer", answer.toString());


        } catch (JSONException e){
            e.printStackTrace();
        }

        _Booking.BookingResponse(data, booking);
    }


    public LinearLayout GridRanking(String key, String title){
        final LinearLayout BuilderLayout = new LinearLayout(mContext);
        BuilderLayout.setOrientation(LinearLayout.VERTICAL);
        BuilderLayout.setPadding(0,20,0,20);

        //TITLE
        final TextView TextTitle = new TextView(mContext);
        TextTitle.setTextColor(Color.WHITE);
        TextTitle.setText(title);
        BuilderLayout.addView(TextTitle);

        //KEY
        final TextView TextID = new TextView(mContext);
        TextID.setId(R.id.RatingGridKey);
        TextID.setTextColor(Color.WHITE);
        TextID.setText(key);
        TextID.setVisibility(View.GONE);
        BuilderLayout.addView(TextID);

        //VALUE
        final EditText TextSelect = new EditText(mContext);
        TextSelect.setId(R.id.RatingGridValue);
        TextSelect.setTextColor(Color.WHITE);
        TextSelect.setText("5");
        TextSelect.setVisibility(View.GONE);
        BuilderLayout.addView(TextSelect);

        //GRID
        final GridView Grid;
        final String[] gridViewString = {"", "", "", "", ""} ;
        int[] gridViewImageId = {R.drawable.ic_star, R.drawable.ic_star, R.drawable.ic_star, R.drawable.ic_star, R.drawable.ic_star};

        Class_GridviewCustom adapterView = new Class_GridviewCustom(mContext, gridViewString, gridViewImageId);
        Grid = new GridView(mContext);
        Grid.setAdapter(adapterView);
        Grid.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,150));
        Grid.setPadding(0, 20, 0, 20);
        Grid.setNumColumns(5);

        //CONFIG
        Grid.post(new Runnable() {
            @Override
            public void run() {
                int select = Integer.parseInt(String.valueOf(TextSelect.getText()));

                final int size = Grid.getChildCount();
                for(int i = 0; i < size; i++) {
                    ViewGroup gridChild = (ViewGroup) Grid.getChildAt(i);
                    int childSize = gridChild.getChildCount();
                    for(int k = 0; k < childSize; k++) {

                        //TEXT
                        if( gridChild.getChildAt(k) instanceof TextView ) {


                        //ICON
                        }else if(gridChild.getChildAt(k) instanceof ImageView){
                            int width=80;
                            int height=80;

                            ImageView imageView = (ImageView)  gridChild.getChildAt(k);
                            imageView.setPadding(0, 0, 0, 0);
                            imageView.setColorFilter(Color.GRAY);

                            if(i<select){
                                imageView.setColorFilter(Color.YELLOW);
                            }

                            LinearLayout.LayoutParams Params;
                            Params = new LinearLayout.LayoutParams(width, height);
                            Params.setMargins(0, 0, 0, 0);
                            imageView.setLayoutParams(Params);
                        }

                    }
                }
            }
        });

        Grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                View GridSelect = parent.getChildAt(i);

                int select = i+1;
                TextSelect.setText(String.valueOf(select));

                final int size = Grid.getChildCount();
                for(int r = 0; r < size; r++) {
                    ViewGroup gridChild = (ViewGroup) Grid.getChildAt(r);
                    int childSize = gridChild.getChildCount();
                    for(int k = 0; k < childSize; k++) {

                        if(gridChild.getChildAt(k) instanceof ImageView){

                            ImageView imageView = (ImageView)  gridChild.getChildAt(k);
                            imageView.setColorFilter(Color.GRAY);

                            if(r<select){
                                imageView.setColorFilter(Color.YELLOW);
                            }
                        }

                    }
                }


            }
        });



        BuilderLayout.addView(Grid);

        //SEPARATOR
        final View ViewSeparator = new View(mContext); //, null, R.style.CustomStyleSeparator
        ViewSeparator.setBackgroundResource(R.color.colorPrimaryDark);
        ViewSeparator.setPadding(0,10,0,10);
        ViewSeparator.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,2));
        BuilderLayout.addView(ViewSeparator);
        return BuilderLayout;
    }

}
