package com.etprogroup.suazz;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityServicesData extends ActivityRoot {
    public Activity mContext;

    //GRID DATE
    public View GridViewSelect;
    final int Style = R.style.CustomStyleAlertDialog;
    final int StyleDatePicker = R.style.CustomStyleDatePicker;
    final int StyleTimePicker = R.style.CustomStyleTimePicker;

    //DATA USER DEFAULT
    Class_User _User;
    public String ServiceType;
    public Calendar ServiceDate;
    public int ServiceQuantity;

    //SERVICES
    Class_Services _Services;
    public JSONObject services;

    //TEXTVIEW
    View ServiceData;
    TextView ServiceDataDate;
    TextView ServiceDataHour;
    TextView ServiceDataQuantity;
    TextView ServiceDataSelect;

    //BUTTON
    public Button ButtonApply;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicesdata);
        mContext = this;

        //HEADER SETPS
        new Class_HeaderSteps(this, "services");

        //USER
        _User = ((App)getApplicationContext()).USERData();
        ServiceType = _User.GetDataServiceType();
        ServiceDate = _User.GetDataServiceDate();
        ServiceQuantity = _User.GetDataServiceQuantity();
        _User.UserCheck(mContext);

        //SERVICES
        _Services = ((App)getApplicationContext()).Services();
        services = _Services.GetServicesRedirect(mContext);

        //Button Data
        ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //Toast.makeText(ActivityServicesData.this, "cambiaria de layout", Toast.LENGTH_LONG).show();
                Intent ServiceIntent = new Intent(view.getContext(), ActivityAutos.class);
                startActivityForResult(ServiceIntent, 0);
            }

        });

        FillData();
    }

    public void FillData(){
        SimpleDateFormat GridFormatDate = new SimpleDateFormat("MM/dd/yy", Locale.US);
        SimpleDateFormat GridFormatHour = new SimpleDateFormat("HH:mm", Locale.US);//:ss
        ServiceData = mContext.findViewById(R.id.LayoutScroll);

        ServiceDataSelect = (TextView) mContext.findViewById(R.id.TextServiceSelect);
        ServiceDataSelect.setText(_Services.GET_Data(ServiceType, "title"));
        ServiceDataSelect.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
                Intent ServiceIntent = new Intent(view.getContext(), ActivityServices.class);
                startActivityForResult(ServiceIntent, 0);
            }
        });

        ServiceDataDate = (TextView) mContext.findViewById(R.id.TextServiceDataDate);
        ServiceDataDate.setText(GridFormatDate.format(ServiceDate.getTime())+"\n"+mContext.getString(R.string.ActivityServiceDataDateOther));
        ServiceDataDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog DatePicker = new DatePickerDialog(ActivityServicesData.this, StyleDatePicker, CallBackDate,
                        ServiceDate.get(Calendar.YEAR),
                        ServiceDate.get(Calendar.MONTH),
                        ServiceDate.get(Calendar.DAY_OF_MONTH));
                DatePicker.setTitle(mContext.getString(R.string.ActivityServiceDataDate));
                DatePicker.show();
            }
        });

        ServiceDataHour = (TextView) mContext.findViewById(R.id.TextServiceDataHour);
        ServiceDataHour.setText(GridFormatHour.format(ServiceDate.getTime()));
        ServiceDataHour.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog TimePicker = new TimePickerDialog(ActivityServicesData.this, StyleTimePicker, CallBackTime,
                        ServiceDate.get(Calendar.HOUR_OF_DAY),
                        ServiceDate.get(Calendar.MINUTE),true);
                TimePicker.setTitle(mContext.getString(R.string.ActivityServiceDataHour));
                TimePicker.show();
            }
        });

        ServiceDataQuantity = (TextView) mContext.findViewById(R.id.TextServiceDataQuantity);
        ServiceDataQuantity.setText(String.valueOf(ServiceQuantity));
        ServiceDataQuantity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityServicesData.this, Style);
                builder.setTitle(mContext.getString(R.string.ActivityServiceDataQuantity));

                final EditText input = new EditText(ActivityServicesData.this);
                input.setInputType(InputType.TYPE_CLASS_NUMBER);//InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD
                input.setText(String.valueOf(ServiceQuantity), TextView.BufferType.EDITABLE);
                input.setTextColor(Color.WHITE);
                input.setFocusable(true);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = input.getText().toString();
                        if(text==""){
                            text= "1";
                        }
                        ServiceQuantity = Integer.parseInt(text);
                        _User.SetDataServiceQuantity(ServiceQuantity);
                        SetText(ServiceData, R.id.TextServiceDataQuantity, text);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
    }

    final DatePickerDialog.OnDateSetListener CallBackDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            ServiceDate.set(Calendar.YEAR, year);
            ServiceDate.set(Calendar.MONTH, monthOfYear);
            ServiceDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            GridDateUpdate(ServiceData, R.id.TextServiceDataDate);
        }
    };


    final TimePickerDialog.OnTimeSetListener CallBackTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
            ServiceDate.set(Calendar.HOUR_OF_DAY, selectedHour);
            ServiceDate.set(Calendar.MINUTE, selectedMinute);
            GridTimeUpdate(ServiceData, R.id.TextServiceDataHour);
        }
    };


    private void GridDateUpdate(View view, int element) {
        ServiceDate = _User.SetDataServiceDate(ServiceDate, mContext);
        String format = "MM/dd/yy";
        SimpleDateFormat formatted = new SimpleDateFormat(format, Locale.US);
        SetText(view, element, formatted.format(ServiceDate.getTime()));
    }


    private void GridTimeUpdate(View view, int element) {
        ServiceDate = _User.SetDataServiceDate(ServiceDate, mContext);
        String format = "HH:mm"; // :ss
        SimpleDateFormat formatted = new SimpleDateFormat(format);
        SetText(view, element, formatted.format(ServiceDate.getTime()));
    }


    private void SetText(View view, int element, String text) {
        //Toast.makeText(this, text, Toast.LENGTH_LONG).show();
        TextView textView = (TextView) view.findViewById(element);
        textView.setText(text);
    }



}
