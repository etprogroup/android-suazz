package com.etprogroup.suazz;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Gomez on 13-11-2017.
 */

public class Class_Providers {
    Activity ActContext;
    Context mContext;
    public JSONObject Providers = new JSONObject();
    public String ProviderSelect = "";
    public String ProviderDevice = "";
    public Class_Locations _Locations;
    public double latitude = 0;
    public double longitude = 0;
    public int distance = 5;
    final int Style = R.style.CustomStyleAlertDialog;

    public Class_Providers(Application mContext) {
    }


    public void GetMarkerProvidersServer(final Activity context, Class_User _User, final Runnable callback) {
        _Locations =  ((App)context.getApplicationContext()).Locations();
        latitude = _Locations.latitude;
        longitude = _Locations.longitude;
        distance = _Locations.distance;

        //PROVIDER DATA
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "providers");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("latitude", String.valueOf(latitude));
            data.put("longitude", String.valueOf(longitude));
            data.put("radio", String.valueOf(distance));

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new Class_General(context).CreateMenssage("Providers", output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new Class_General(mContext).CreateMenssage("success", message);
                        //Log.d("providers",response.getString("providers"));
                        Providers = response.getJSONObject("providers");
                        callback.run();

                    }else if(result.equals("notfound")){
                        String title = response.getString("title");
                        Providers = new JSONObject();

                        AlertDialog.Builder builder = new AlertDialog.Builder(context, Style);
                        builder.setTitle(title);
                        builder.setMessage(message);

                        builder.setPositiveButton(context.getString(R.string.ActivityMapAlertDialogNewArea), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent Intent = new Intent(context, ActivityMaps.class);
                                context.startActivity(Intent);
                            }
                        });

                        builder.setNegativeButton(context.getString(R.string.AlertDialogNegative), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(context, ActivityMain.class);
                        context.startActivity(Intent);

                    }else{
                        new Class_General(context).CreateMenssage("Error", message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataServer(context, data, response).execute("");
    }

    public JSONObject GetMarkerProviders() {
        int factor = 1;
        for (int id = 1; id < 10; id++) {
            try {
                JSONObject Provider = new JSONObject();
                Provider.put("id", id);
                Provider.put("image", "");
                Provider.put("title", "Title" + id);
                Provider.put("ranking", "Ranking" + id);
                Provider.put("agency", "Agencia" + id);
                Provider.put("phone", "Telefono" + id);
                Provider.put("time", "30 a 40 " + id);
                Provider.put("detail", "Descripcion" + id);
                Provider.put("latitude", latitude + (2 * id * factor / 10));
                Provider.put("longitude", longitude + (2 * id * factor / 10));
                Providers.put(String.valueOf(id), Provider);
                factor = factor * (-1);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return Providers;
    }
}
