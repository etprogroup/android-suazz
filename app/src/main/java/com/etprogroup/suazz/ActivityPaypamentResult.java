package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityPaypamentResult extends ActivityRoot {
    public Activity mContext;
    public Button ButtonApply;
    public Class_User _User;
    String PaymentTitle;
    String PaymentPrice;
    String PaymentAmount;
    String PaypalCurrency;
    String PaymentDetails;
    String PaymentMessage;
    String PaymentResult;
    String PaymentResultCode;
    String PaymentProvider;
    String PaymentService;
    String PaymentAutos;
    String PaymentAutosCount;
    String PaymentDate;
    String PaymentDateTimeStamp;
    String booking;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypamentresult);
        mContext = this;

        //HEADER SETPS
        new Class_HeaderSteps(this, "payments");

        //USER
        _User = ((App)getApplicationContext()).USERData();

        Intent data = getIntent();
        booking = data.getStringExtra("booking");
        PaymentTitle = data.getStringExtra("PaymentTitle");
        PaymentPrice = data.getStringExtra("PaymentPrice");
        PaymentAmount = data.getStringExtra("PaymentAmount");
        PaypalCurrency = data.getStringExtra("PaypalCurrency");
        PaymentDetails = data.getStringExtra("PaymentDetails");
        PaymentMessage = data.getStringExtra("PaymentMessage");
        PaymentResult = data.getStringExtra("PaymentResult");
        PaymentResultCode = data.getStringExtra("PaymentResultCode");
        PaymentProvider = data.getStringExtra("PaymentProvider");
        PaymentService = data.getStringExtra("PaymentService");
        PaymentAutos = data.getStringExtra("PaymentAutos");
        PaymentAutosCount = data.getStringExtra("PaymentAutosCount");
        PaymentDate = data.getStringExtra("PaymentDate");
        PaymentDateTimeStamp = data.getStringExtra("PaymentDateTimeStamp");

        FillData();
        UploadInfo();
    }

    public void FillData(){

        TextView Title = (TextView) findViewById(R.id.TextPaypamentResultTitle);
        Title.setText(PaymentTitle);

        TextView Amount = (TextView) findViewById(R.id.TextPaypamentResultAmount);
        Amount.setText(PaypalCurrency+" "+PaymentAmount);

        TextView Result = (TextView) findViewById(R.id.TextPaypamentResultText);
        Result.setText(PaymentResult);

        TextView Message = (TextView) findViewById(R.id.TextPaypamentResultMessage);
        Message.setText(PaymentMessage);

        TextView Detail = (TextView) findViewById(R.id.TextPaypamentResultDetail);
        Detail.setVisibility(View.GONE);
        Detail.setText(PaymentDetails);


        Button ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setVisibility(View.VISIBLE);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent Intent = new Intent(mContext, ActivityProfileBookingActive.class);
                startActivity(Intent);
            }

        });


    }



    //UPLOAD INFO
    public void UploadInfo() {

        final JSONObject data = new JSONObject();
        try {
            data.put("action", "paypament");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("booking", booking);
            data.put("title", PaymentTitle);
            data.put("provider", PaymentProvider);
            data.put("service", PaymentService);
            data.put("price", PaymentPrice);
            data.put("autoscount", PaymentAutosCount);
            data.put("autos", PaymentAutos);
            data.put("date", PaymentDate);
            data.put("datetime", PaymentDateTimeStamp);
            data.put("amount", PaymentAmount);
            data.put("currency", PaypalCurrency);
            data.put("details", PaymentDetails);
            data.put("message", PaymentMessage);
            data.put("result", PaymentResult);
            data.put("resultcode", PaymentResultCode);

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse UserResponse = new Class_DataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new Class_General(mContext).CreateMenssage("Update", output);
                //Log.d("Update",output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new Class_General(mContext).CreateMenssage("Message", message);

                        //Badges
                        new Class_DataStorage(mContext, "write", "badges", response.getString("badges")).execute("");

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new Class_General(mContext).CreateMenssage("Error", message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        new Class_DataServer(mContext, data, UserResponse).execute("");

    }

}
