package com.etprogroup.suazz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityAutos extends ActivityRoot {
    private Context mContext;
    private Class_User _User;
    private Class_Autos _UserAutos;
    private JSONObject UserAutos;

    private Button ButtonApply;
    private ListView ListviewAutos;
    private LinearLayout ExtraOptions;

    private View ListViewSelect;
    private final int Style = R.style.CustomStyleAlertDialog;
    private Class_ListViewAutos adapterViewAutos;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autos);
        mContext = this;

        //HEADER SETPS
        new Class_HeaderSteps(this, "autos");

        //OPTIONS
        ExtraOptions = (LinearLayout) findViewById(R.id.extraoptions);

        //Button
        ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view){
                UpdateAllAutos(view);
                Intent intent = new Intent(view.getContext(), ActivityMaps.class);
                startActivityForResult(intent, 0);
            }
        });

        //DATA
        _User = ((App)getApplicationContext()).USERData();
        _UserAutos = ((App)getApplicationContext()).Autos();
        UserAutos = _UserAutos.GetDataUserAutos();
        _User.UserCheck(mContext);

        //UserAutos = new JSONObject(_User.GET_Data("autos","value"));
        UserAutos = _UserAutos.GetDataStorage_Autos(mContext, new Runnable() {
            public void run() {
                UserAutos = _UserAutos.GetDataUserAutos();
                //Log.d("UserAutostoString",UserAutos.toString());
                Create_GridItemsAutos();
            }
        });
    }

    public void Create_GridItemsAutos() {
        adapterViewAutos = new Class_ListViewAutos(mContext, UserAutos);
        ListviewAutos=(ListView) findViewById(R.id.ListAutos);
        ListviewAutos.setAdapter(adapterViewAutos);
        RefreshListView();

        //CONFIG
        ListviewAutos.post(new Runnable() {
            @Override
            public void run() {
                //ListviewAutos.setItemsCanFocus(true);
                final int size = ListviewAutos.getChildCount();
                /*
                for(int i = 0; i < size; i++) {
                    TextView textView = (TextView) ListAutos.getChildAt(i);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                    textView.setTextColor(Color.WHITE);
                    textView.setGravity(Gravity.CENTER);
                }
                */
            }
        });


        ListviewAutos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                //Toast.makeText(getApplicationContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();

                ListViewSelect = parent.getChildAt(position);
                TextView textViewID = (TextView) ListViewSelect.findViewById(R.id.textID);
                TextView textViewAuto = (TextView) ListViewSelect.findViewById(R.id.textAuto);
                TextView textViewBrand = (TextView) ListViewSelect.findViewById(R.id.textBrand);
                TextView textViewModel = (TextView) ListViewSelect.findViewById(R.id.textModel);
                TextView textViewLicense = (TextView) ListViewSelect.findViewById(R.id.textLicenseplate);
                Log.d("ListView getHeight", String.valueOf(ListViewSelect.getHeight()));

                final String textID = (String) textViewID.getText();

                CheckBox checkBox = (CheckBox) ListViewSelect.findViewById(R.id.CheckBox);
                boolean itemChecked = checkBox.isChecked();
                _UserAutos.SET_DataByPosition(position, String.valueOf(itemChecked), "selected");

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, Style);
                builder.setTitle(getString(R.string.ActivityAutoSelectTitle));
                builder.setMessage(getString(R.string.ActivityAutoSelectMessage));

                LinearLayout BuilderLayout = CreateLayoutAutos(String.valueOf(textViewBrand.getText()), String.valueOf(textViewModel.getText()), String.valueOf(textViewLicense.getText()));
                final EditText inputbrand = (EditText) BuilderLayout.findViewById(R.id.AutoBrand);
                final EditText inputmodel = (EditText) BuilderLayout.findViewById(R.id.AutoModel);
                final EditText inputplate = (EditText) BuilderLayout.findViewById(R.id.AutoPlate);
                builder.setView(BuilderLayout);

                builder.setPositiveButton(getString(R.string.ActivityAutoButtonUpdate), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UpdateAllAutos(view);
                        String textinputbrand = inputbrand.getText().toString();
                        String textinputmodel = inputmodel.getText().toString();
                        String textinputplate = inputplate.getText().toString();

                        try {
                            JSONObject auto = UserAutos.getJSONObject(textID);
                            auto.put("brand", textinputbrand);
                            auto.put("model", textinputmodel);
                            auto.put("licenseplate", textinputplate);

                            if(!textinputbrand.equals("") && !textinputmodel.equals("")){
                                _UserAutos.SetJSONDataUserAutos(textID, auto);
                                UserAutos = _UserAutos.GetDataUserAutos();
                                RefreshListView();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton(getString(R.string.ActivityAutoButtonRemove), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UpdateAllAutos(view);
                        UserAutos = _UserAutos.DELETE_DataByID(textID);
                        RefreshListView();
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });
    }



    public void AddListAuto(final View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, Style);
        builder.setTitle(getString(R.string.ActivityAutoSelectTitle));
        builder.setMessage(getString(R.string.ActivityAutoSelectMessage));

        LinearLayout BuilderLayout = CreateLayoutAutos("", "", "");
        final EditText inputbrand = (EditText) BuilderLayout.findViewById(R.id.AutoBrand);
        final EditText inputmodel = (EditText) BuilderLayout.findViewById(R.id.AutoModel);
        final EditText inputplate = (EditText) BuilderLayout.findViewById(R.id.AutoPlate);
        builder.setView(BuilderLayout);

        builder.setPositiveButton(getString(R.string.ActivityAutoButtonPositive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                UpdateAllAutos(view);
                String textinputbrand = inputbrand.getText().toString();
                String textinputmodel = inputmodel.getText().toString();
                String textinputplate = inputplate.getText().toString();

                if(!textinputbrand.equals("") && !textinputmodel.equals("")){
                    _UserAutos.AddDataUserAutos(String.valueOf(0), textinputbrand, textinputmodel, textinputplate);
                }

                UserAutos = _UserAutos.GetDataUserAutos();
                RefreshListView();
            }
        });
        builder.setNegativeButton(getString(R.string.ActivityAutoButtonNegative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private LinearLayout CreateLayoutAutos(String brand, String model, String plate) {

        LinearLayout.LayoutParams params =
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1);

        LinearLayout BuilderLayout = new LinearLayout(mContext);
        BuilderLayout.setOrientation(LinearLayout.VERTICAL);
        BuilderLayout.setPadding(40,0,40,0);


        //BRAND
        LinearLayout LayoutBrand = new LinearLayout(mContext);
        LayoutBrand.setOrientation(LinearLayout.HORIZONTAL);

        final TextView TextBrand = new TextView(mContext);
        TextBrand.setTextColor(Color.WHITE);
        TextBrand.setText(mContext.getString(R.string.ActivityAutoBrand));
        LayoutBrand.addView(TextBrand);

        final EditText inputbrand = new EditText(mContext);
        inputbrand.setId(R.id.AutoBrand);
        inputbrand.setInputType(InputType.TYPE_CLASS_TEXT);// InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD
        inputbrand.setTextAppearance(mContext, R.style.TextInputLayout);
        inputbrand.setHintTextColor(Color.WHITE);
        inputbrand.setTextColor(Color.WHITE);
        inputbrand.setText(brand, TextView.BufferType.EDITABLE);
        //inputbrand.setHint(mContext.getString(R.string.ActivityAutoBrand));
        inputbrand.setLayoutParams(params);
        LayoutBrand.addView(inputbrand);


        //MODEL
        LinearLayout LayoutModel = new LinearLayout(mContext);
        LayoutModel.setOrientation(LinearLayout.HORIZONTAL);

        final TextView TextModel = new TextView(mContext);
        TextModel.setTextColor(Color.WHITE);
        TextModel.setText(mContext.getString(R.string.ActivityAutoModel));
        LayoutModel.addView(TextModel);

        final EditText inputmodel = new EditText(mContext);
        inputmodel.setId(R.id.AutoModel);
        inputmodel.setTextAppearance(mContext, R.style.TextInputLayout);
        inputmodel.setInputType(InputType.TYPE_CLASS_TEXT);
        inputmodel.setHintTextColor(Color.WHITE);
        inputmodel.setTextColor(Color.WHITE);
        inputmodel.setText(model, TextView.BufferType.EDITABLE);
        //inputmodel.setHint(mContext.getString(R.string.ActivityAutoModel));
        inputmodel.setLayoutParams(params);
        LayoutModel.addView(inputmodel);


        //PLATE
        LinearLayout LayoutPlate = new LinearLayout(mContext);
        LayoutPlate.setOrientation(LinearLayout.HORIZONTAL);

        final TextView TextPlate = new TextView(mContext);
        TextPlate.setTextColor(Color.WHITE);
        TextPlate.setText(mContext.getString(R.string.ActivityAutoLicensePlate));
        LayoutPlate.addView(TextPlate);

        final EditText inputplate = new EditText(mContext);
        inputplate.setId(R.id.AutoPlate);
        inputplate.setTextAppearance(mContext, R.style.TextInputLayout);
        inputplate.setInputType(InputType.TYPE_CLASS_TEXT);
        inputplate.setHintTextColor(Color.WHITE);
        inputplate.setTextColor(Color.WHITE);
        inputplate.setText(plate, TextView.BufferType.EDITABLE);
        //inputlicenseplate.setHint(mContext.getString(R.string.ActivityAutoLicensePlate));
        inputplate.setLayoutParams(params);
        LayoutPlate.addView(inputplate);

        BuilderLayout.addView(LayoutBrand);
        BuilderLayout.addView(LayoutModel);
        BuilderLayout.addView(LayoutPlate);
        return BuilderLayout;
    }

    private void UpdateAllAutos(View view) {
        JSONObject AutosSelect = new JSONObject();
        int ListSize = ListviewAutos.getChildCount();
        for (int position = 0; position < ListSize; position++ ){
            View ListItem = ListviewAutos.getChildAt(position);

            TextView textViewID = (TextView) ListItem.findViewById(R.id.textID);
            TextView textViewBrand = (TextView) ListItem.findViewById(R.id.textBrand);
            TextView textViewModel = (TextView) ListItem.findViewById(R.id.textModel);
            TextView textViewLicense = (TextView) ListItem.findViewById(R.id.textLicenseplate);
            CheckBox checkBox = (CheckBox) ListItem.findViewById(R.id.CheckBox);

            String textID = (String) textViewID.getText();
            String textBrand = (String) textViewBrand.getText();
            String textModel = (String) textViewModel.getText();
            String textLicense = (String) textViewLicense.getText();
            boolean itemChecked = checkBox.isChecked();

            _UserAutos.SET_DataByID(textID, textBrand, "brand");
            _UserAutos.SET_DataByID(textID, textModel, "model");
            _UserAutos.SET_DataByID(textID, textLicense, "licenseplate");
            _UserAutos.SET_DataByID(textID, String.valueOf(itemChecked), "selected");

            if(itemChecked){
                String Auto = String.format("ID: %s, %s, %s, %s", textID, mContext.getString(R.string.ActivityAutoBrand)+": "+textBrand, mContext.getString(R.string.ActivityAutoModel)+": "+textModel, mContext.getString(R.string.ActivityAutoLicensePlate)+": "+textLicense);

                try {
                    AutosSelect.put(String.valueOf(textID), Auto);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        //Log.d("Autos", Autos.toString());
        _UserAutos.SetDataUserAutosSelect(AutosSelect);
        _UserAutos.SetDataStorage_Autos(mContext, UserAutos);
    }

    public void RefreshListView(){
        int Quantity = UserAutos.length();

        ListAdapter listAdapter = ListviewAutos.getAdapter();
        if(listAdapter == null) {
            return;
        }

        int Height = 0;
        // int desiredWidth = MeasureSpec.makeMeasureSpec(listview.getWidth(), MeasureSpec.AT_MOST);
        for(int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, ListviewAutos);
            //listItem.measure(0, 0);
            listItem.measure(
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            Height += listItem.getMeasuredHeight();
        }

        Height += ListviewAutos.getDividerHeight()*(listAdapter.getCount()-1);
        ViewGroup.LayoutParams params = ListviewAutos.getLayoutParams();
        params.height = Height;
        ListviewAutos.setLayoutParams(params);
        //ListviewAutos.setExpand(true);


        adapterViewAutos.notifyDataSetChanged();

        if(Quantity > 4){
            ExtraOptions.setVisibility(View.VISIBLE);
        }else{
            ExtraOptions.setVisibility(View.GONE);
        }

        //Log.d("Autos", Autos.toString());
        _UserAutos.SetDataStorage_Autos(mContext, UserAutos);
    }

}
