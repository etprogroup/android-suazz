package com.etprogroup.suazz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Gomez on 08-11-2017.
 */

public class ActivityProfileBookingProviderPending extends AppCompatActivity {
    public Activity mContext;
    public Class_User _User;
    public Class_Booking _Booking;

    public ListView Listview;
    public View ListViewSelect;
    final int Style = R.style.CustomStyleAlertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookings);
        mContext = this;

        //TOOLBAR
        new Class_Toolbar(mContext);

        //TITLE
        TextView TextTitle = (TextView) findViewById(R.id.TextBooking);
        TextTitle.setText(getString(R.string.ActivityProfileBookingPendingComfirm));

        _Booking = new Class_Booking(mContext);
        _User = ((App)getApplicationContext()).USERData();
        _User.UserCheck(mContext, ActivityProfileBookingProviderPending.class);


        Bookings();
        final SwipeRefreshLayout SwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.SwipeRefreshLayout);
        SwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        SwipeRefreshLayout.setRefreshing(false);
                        Bookings();
                    }
                }
        );
    }


    private void Bookings(){
        Runnable Callback = new Runnable() {
            @Override
            public void run() {
                BookingAction();
            }
        };

        try {
            //BOOKING DATA
            final JSONObject data = new JSONObject();
            data.put("action", "bookings");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("type", "wait_provider");
            _Booking.FillBooking(data, Callback);

        } catch (JSONException e){
            e.printStackTrace();
        }
    }


    private void BookingAction() {
        Listview = _Booking.Listview;
        Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                ListViewSelect = parent.getChildAt(position);
                Log.d("ListView getHeight", String.valueOf(ListViewSelect.getHeight()));

                final TextView textViewID = (TextView) ListViewSelect.findViewById(R.id.textID);
                final String textID = (String) textViewID.getText();

                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, Style);
                builder.setTitle(getString(R.string.ActivityProfileBookingSelectTitle));
                builder.setMessage(getString(R.string.ActivityProfileBookingSelectMessage));

                LinearLayout BuilderLayout = new LinearLayout(mContext);
                BuilderLayout.setOrientation(LinearLayout.VERTICAL);
                BuilderLayout.setPadding(40,0,40,0);

                //DETAIL
                //BuilderLayout = CreateBuilderLayout(BuilderLayout, textID);
                //builder.setView(BuilderLayout);

                builder.setPositiveButton(getString(R.string.ActivityProfileBookingPositive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BookingResponse(textID,"approved");

                    }
                });

                builder.setNegativeButton(getString(R.string.ActivityProfileBookingNegative), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ListViewSelect.setBackground(getColor(R.color.colorPrimaryBack));
                        BookingResponse(textID,"cancelled");
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });
    }

    private void BookingResponse(String booking, String action) {
        //BOOKING DATA
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "booking_response");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("booking", booking);
            data.put("option", action);


        } catch (JSONException e){
            e.printStackTrace();
        }

        _Booking.BookingResponse(data, booking);
    }


    private LinearLayout CreateBuilderLayout(LinearLayout BuilderLayout, String textID) {
        TextView TextService = (TextView) ListViewSelect.findViewById(R.id.TextService);
        TextView TextDate = (TextView) ListViewSelect.findViewById(R.id.TextDate);
        TextView TextHour = (TextView) ListViewSelect.findViewById(R.id.TextHour);

        //DETAIL
        try {
            JSONObject Booking = _Booking.Bookings.getJSONObject(textID);
            String token = Booking.getString("token");
            String title = Booking.getString("title");

            final TextView TextViewService = (TextView) new TextView(mContext);
            TextViewService.setText(TextService.getText());
            //TextViewService.setTextColor(Color.WHITE);
            BuilderLayout.addView(TextViewService);

            final TextView TextViewDate = (TextView) new TextView(mContext);
            TextViewDate.setText(TextDate.getText());
            BuilderLayout.addView(TextViewDate);

            final TextView TextViewHour = (TextView) new TextView(mContext);
            TextViewHour.setText(TextHour.getText());
            BuilderLayout.addView(TextViewHour);

            final TextView TextViewquantity = (TextView) new TextView(mContext);
            TextViewquantity.setText(Booking.getString("quantity"));
            BuilderLayout.addView(TextViewquantity);

            final TextView TextViewautos = (TextView) new TextView(mContext);
            TextViewautos.setText(Booking.getString("autos"));
            BuilderLayout.addView(TextViewautos);

            final TextView TextViewlatitude = (TextView) new TextView(mContext);
            TextViewlatitude.setText(Booking.getString("latitude"));
            BuilderLayout.addView(TextViewlatitude);

            final TextView TextViewlongitude = (TextView) new TextView(mContext);
            TextViewlongitude.setText(Booking.getString("longitude"));
            BuilderLayout.addView(TextViewlongitude);

            final TextView TextViewdatetime = (TextView) new TextView(mContext);
            TextViewdatetime.setText(Booking.getString("datetime"));
            BuilderLayout.addView(TextViewdatetime);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return BuilderLayout;
    }

}
