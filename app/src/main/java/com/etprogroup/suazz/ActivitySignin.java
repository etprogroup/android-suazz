package com.etprogroup.suazz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivitySignin extends ActivityRoot {
    private Context mContext;
    private EditText usermail;
    private EditText userpass;
    private EditText usermailpass;
    private Class_User _User;
    private LinearLayout LayoutLogin;
    private LinearLayout LayoutPass;
    private final int Style = R.style.CustomStyleAlertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        mContext = this;

        _User = ((App)getApplicationContext()).USERData();
        usermail = (EditText) findViewById(R.id.EditEmail);
        userpass = (EditText) findViewById(R.id.EditPass);
        usermailpass = (EditText) findViewById(R.id.EditEmailPass);

        LayoutLogin = (LinearLayout) findViewById(R.id.LayoutLogin);
        LayoutPass = (LinearLayout) findViewById(R.id.LayoutPass);

        //LOGIN
        Button ButtonApplyLogin = (Button) findViewById(R.id.ButtonApplyLogin);
        ButtonApplyLogin.setEnabled(true);
        ButtonApplyLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LogIn();
            }
        });

        //PASS
        Button ButtonApplyPass = (Button) findViewById(R.id.ButtonApplyPass);
        ButtonApplyPass.setEnabled(true);
        ButtonApplyPass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ForgotPass();
            }
        });

        //SIGNIN
        TextView TextSignUp = (TextView) findViewById(R.id.TextSignUp);
        TextSignUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent Intent = new Intent(view.getContext(), ActivitySigninData.class);
                startActivity(Intent);
            }

        });


        //ACTIVITY
        TextView TextForgotPass = (TextView) findViewById(R.id.TextForgotPass);
        TextForgotPass.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ActivityForgotPass(true);
            }
        });

        TextView TextSigninLogIn = (TextView) findViewById(R.id.TextSigninLogIn);
        TextSigninLogIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ActivityForgotPass(false);
            }
        });
    }


    private void LogIn(){
        JSONObject data = new JSONObject();
        try {
            data.put("action", "login");
            data.put("user", usermail.getText().toString());
            data.put("pass", userpass.getText().toString());

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new Class_General(mContext).CreateMenssage("response", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");
                    String title = response.getString("title");

                    if(result.equals("success")){
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        _User.SetDataData(response.getString("data"));

                        JSONObject user = new JSONObject();
                        user.put("token", _User.GetDataToken());
                        user.put("user", _User.GetDataUser());
                        user.put("data", _User.GetDataData());

                        Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

                            @Override
                            public void processFinish(String output){
                                //new Class_General(mContext).CreateMenssage("response", output);
                                Intent Intent = new Intent(mContext, ActivityMain.class);
                                startActivity(Intent);
                            }
                        };

                        new Class_DataStorage(mContext, "write", "user", user.toString(), Callback).execute("");


                    }else if(result.equals("validate")){
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        new Class_General(mContext).CreateMenssage(title, message);

                        Intent Intent = new Intent(mContext, ActivitySigninConfirmed.class);
                        startActivity(Intent);

                    }else if(result.equals("error")){
                        new Class_General(mContext).CreateMenssage(title, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataServer(mContext, data, response).execute("");
    }



    private void ActivityForgotPass(Boolean value) {
        if(value){
            LayoutLogin.setVisibility(View.GONE);
            LayoutPass.setVisibility(View.VISIBLE);

        }else{
            LayoutLogin.setVisibility(View.VISIBLE);
            LayoutPass.setVisibility(View.GONE);
        }
    }

    private void ForgotPass(){

        final JSONObject data = new JSONObject();
        try {
            data.put("action", "password");
            data.put("option", "recover");
            data.put("device", _User.GetDataDevice());
            data.put("user", usermailpass.getText().toString());

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new Class_General(mContext).CreateMenssage("response", output);
                try {
                    final JSONObject response = new JSONObject(output);
                    final String result = response.getString("result");
                    final String message = response.getString("message");
                    final String title = response.getString("title");

                    if(result.equals("success")){
                        final String user = response.getString("user");
                        ForgotPassAlertCode(title, message, user);


                    }else if(result.equals("compatible")){
                        _User.Check = true;
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        _User.SetDataData(response.getString("data"));
                        _User.SetDataImage(response.getString("image"));

                        JSONObject StorageUser = new JSONObject();
                        StorageUser.put("token", _User.GetDataToken());
                        StorageUser.put("user", _User.GetDataUser());
                        StorageUser.put("data", _User.GetDataData());
                        StorageUser.put("providers", "{}");

                        Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

                            @Override
                            public void processFinish(String output){
                                //new Class_General(mContext).CreateMenssage("SAVE", output);
                                Intent Intent = new Intent(mContext, ActivityMain.class);
                                Intent.putExtra("classname", ActivityProfileData.class);
                                startActivity(Intent);
                            }
                        };

                        new Class_DataStorage(mContext, "write", "user", StorageUser.toString(), Callback).execute("");


                    }else if(result.equals("error")){
                        new Class_General(mContext).CreateMenssage(title, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new Class_DataServer(mContext, data, response).execute("");
    }



    private void ForgotPassCode(final String user, String code){

        final JSONObject data = new JSONObject();
        try {
            data.put("action", "password");
            data.put("option", "confirm");
            data.put("code", code);
            data.put("user", user);

        } catch (JSONException e){
            e.printStackTrace();
        }

        final Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new Class_General(mContext).CreateMenssage("response", output);
                try {
                    final JSONObject response = new JSONObject(output);
                    final String result = response.getString("result");
                    final String message = response.getString("message");
                    final String title = response.getString("title");

                    if(result.equals("success")){
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        _User.SetDataData(response.getString("data"));

                        JSONObject user = new JSONObject();
                        user.put("token", _User.GetDataToken());
                        user.put("user", _User.GetDataUser());
                        user.put("data", _User.GetDataData());

                        Class_DataStorage.AsyncResponse Callback = new Class_DataStorage.AsyncResponse(){

                            @Override
                            public void processFinish(String output){
                                //new Class_General(mContext).CreateMenssage("response", output);
                                Intent Intent = new Intent(mContext, ActivityMain.class);
                                Intent.putExtra("classname", ActivityProfileData.class);
                                startActivity(Intent);
                            }
                        };

                        new Class_DataStorage(mContext, "write", "user", user.toString(), Callback).execute("");


                    }else if(result.equals("error")){
                        //new Class_General(mContext).CreateMenssage(title, message);
                        ForgotPassAlertCode(title, message, user);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        new Class_DataServer(mContext, data, response).execute("");
    }


    private void ForgotPassAlertCode(final String title, String message, final String user){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, Style);
        builder.setTitle(title);
        builder.setMessage(message);

        //DEATIL
        LinearLayout BuilderLayout = new LinearLayout(mContext);
        BuilderLayout.setOrientation(LinearLayout.VERTICAL);
        BuilderLayout.setPadding(40,0,40,0);
        BuilderLayout.setGravity(View.TEXT_ALIGNMENT_CENTER);

        final EditText inputcode = new EditText(mContext);
        inputcode.setTextAppearance(mContext, R.style.TextInputLayout);
        inputcode.setInputType(InputType.TYPE_CLASS_TEXT);
        inputcode.setHint(getString(R.string.ActivitySigninCodePass));
        BuilderLayout.addView(inputcode);

        builder.setView(BuilderLayout);
        builder.setPositiveButton(getString(R.string.ActivitySigninCodePositiveButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String sinputcode = inputcode.getText().toString();
                ForgotPassCode(user, sinputcode);

            }
        });

        builder.setNegativeButton(getString(R.string.ActivitySigninCodeNegativeButton), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

}
