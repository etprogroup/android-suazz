package com.etprogroup.suazz;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Gomez on 08-11-2017.
 */

public class ActivityProfileBookingPayment extends ActivityRoot {
    public Activity mContext;
    public Class_User _User;
    public Class_Booking _Booking;
    public String booking;

    public ListView Listview;
    public View ListViewSelect;
    final int Style = R.style.CustomStyleAlertDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookings);
        mContext = this;

        //TOOLBAR
        new Class_Toolbar(mContext);

        //TITLE
        TextView TextTitle = (TextView) findViewById(R.id.TextBooking);
        TextTitle.setText(getString(R.string.ActivityProfileBookingPendingPaypament));


        Intent idata = getIntent();
        booking = idata.getStringExtra("option");

        _Booking = new Class_Booking(mContext);
        _User = ((App)getApplicationContext()).USERData();
        _User.UserCheck(mContext, ActivityProfileBookingPayment.class, booking);


        Bookings();
        final SwipeRefreshLayout SwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.SwipeRefreshLayout);
        SwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        SwipeRefreshLayout.setRefreshing(false);
                        Bookings();
                    }
                }
        );
    }


    private void Bookings(){


        Runnable Callback = new Runnable() {
            @Override
            public void run() {
                BookingAction();
            }
        };

        try {
            //BOOKING DATA
            JSONObject data = new JSONObject();
            data.put("action", "bookings");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("type", "wait_paypament");
            _Booking.FillBooking(data, Callback);

        } catch (JSONException e){
            e.printStackTrace();
        }
    }


    private void BookingAction() {
        Listview = _Booking.Listview;
        Listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                ListViewSelect = parent.getChildAt(position);

                final TextView textViewID = (TextView) ListViewSelect.findViewById(R.id.textID);
                final String textID = (String) textViewID.getText();
                //BookingRedirect(textID, _Booking.Bookings.toString());

                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, Style);
                builder.setTitle(getString(R.string.ActivityProfileBookingSelectTitle));
                builder.setMessage(getString(R.string.ActivityProfileBookingPayMessage));

                builder.setPositiveButton(getString(R.string.ActivityProfileBookingPendingPay), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BookingRedirect(textID, _Booking.Bookings.toString());
                    }
                });

                builder.setNegativeButton(getString(R.string.ActivityProfileBookingCancelled), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BookingResponse(textID,"cancelled_user");
                    }
                });

                builder.setNeutralButton(getString(R.string.ActivityProfileBookingClose), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });



        //NOTIFY REDIRECT
        if(booking!=null){
            if(booking!=""){
                BookingRedirect(booking, _Booking.Bookings.toString());
            }
        }

    }

    private void BookingRedirect(String booking, String sbookings) {
        Intent Intent = new Intent(mContext, ActivityPaypamentData.class);
        Intent.putExtra("booking", booking);
        Intent.putExtra("bookings", sbookings);
        mContext.startActivity(Intent);
    }

    private void BookingResponse(String booking, String action) {
        //BOOKING DATA
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "booking_response");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("booking", booking);
            data.put("option", action);


        } catch (JSONException e){
            e.printStackTrace();
        }

        _Booking.BookingResponse(data, booking);
    }

}
