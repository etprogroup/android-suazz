package com.etprogroup.suazz;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Carlos Gomez on 08-11-2017.
 */

public class ActivityGeneralSupport extends ActivityRoot {
    public Activity mContext;
    private JSONObject Support;
    private String SupportTitle;
    private final int Style = R.style.CustomStyleAlertDialog;
    private LinearLayout layout;
    private Class_User _User;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);
        mContext = this;

        //TOOLBAR
        new Class_Toolbar(mContext);

        _User = ((App)getApplicationContext()).USERData();
        _User.UserCheck(mContext, ActivityGeneralSupport.class);

        ///LAYOUT
        layout = (LinearLayout) mContext.findViewById(R.id.ActivityGeneral);
        SupportMail();

        //ICON
        ImageView Icon = (ImageView) findViewById(R.id.imageDetail);
        Icon.setImageResource(R.drawable.ic_ayuda);
        Icon.setVisibility(View.VISIBLE);

        //TITLE
        TextView TextTitle = (TextView) findViewById(R.id.TextDetail);
        TextTitle.setText(getString(R.string.ActivityDetailSupport));


        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        SupportTitle  = response.getString("title");
                        String StringSupport = response.getString("support");
                        Support = new JSONObject(StringSupport);
                        if(Support.length()>0){
                            SupportAction();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        try {
            //SUPPORT
            final JSONObject data = new JSONObject();
            data.put("action", "support");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            new Class_DataServer(mContext, data, response).execute("");

        } catch (JSONException e){
            e.printStackTrace();
        }
    }


    private void SupportAction() {
        int Questions = 0;
        LinearLayout LayoutContent = (LinearLayout) mContext.findViewById(R.id.LayoutContent);
        LayoutContent.setPadding(0,50,0,50);

        final TextView SupportTextTitle = new TextView(mContext);
        SupportTextTitle.setPadding(50,40,50,40);
        SupportTextTitle.setGravity(Gravity.CENTER);
        SupportTextTitle.setTextColor(Color.WHITE);
        SupportTextTitle.setTextSize(22);
        SupportTextTitle.setText(SupportTitle);
        //LayoutContent.addView(SupportTextTitle);
        layout.addView(SupportTextTitle,4);

        //SEPARATE
        layout.addView(ViewSeparate(),5);


        try {
            Iterator<String> iter = Support.keys();
            while(iter.hasNext()) {
                Questions=Questions+1;
                String key = iter.next();

                JSONObject Question = Support.getJSONObject(key);
                String title = Question.getString("title");
                String content = Question.getString("content");

                final LinearLayout BuilderLayout = new LinearLayout(mContext);
                BuilderLayout.setOrientation(LinearLayout.VERTICAL);
                BuilderLayout.setPadding(0,2,0,0);

                //TITLE
                final TextView TextTitle = new TextView(mContext);
                TextTitle.setTextColor(Color.WHITE);
                TextTitle.setTextSize(20);
                TextTitle.setText(Questions+". "+title);
                TextTitle.setPadding(50,50,50,50);
                BuilderLayout.addView(TextTitle);

                //CONTENT
                final TextView TextContent = new TextView(mContext);
                TextContent.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorTransparent, null));
                //TextContent.setBackgroundColor(Color.BLACK);
                TextContent.setTextColor(Color.WHITE);
                TextContent.setVisibility(View.GONE);
                TextContent.setText(content);
                TextContent.setText(new Class_General(mContext).fromHtml(content));
                TextContent.setTextSize(18);
                TextContent.setPadding(50,20,50,20);
                BuilderLayout.addView(TextContent);

                //SEPARATE
                BuilderLayout.addView(ViewSeparate());

                //ADD LAYOUT
                LayoutContent.addView(BuilderLayout);

                BuilderLayout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v){

                        if(TextContent.getVisibility() == View.VISIBLE){
                            TextTitle.setTypeface(null, Typeface.NORMAL);
                            TextContent.setVisibility(View.GONE);
                            /*
                            TextContent.animate()
                                    .translationY(0)
                                    .alpha(0.0f)
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                            TextContent.setVisibility(View.GONE);
                                        }
                                    });
*/
                        }else{
                            TextTitle.setTypeface(null, Typeface.BOLD_ITALIC);
                            TextContent.setVisibility(View.VISIBLE);

                            /*
                            TextContent.setAlpha(0.0f);
                            TextContent.animate()
                                    .translationY(TextContent.getHeight())
                                    .alpha(1.0f)
                                    .setListener(null);
                                    */
                        }
                    }
                });

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void SupportMail() {

        //MAIL
        final LinearLayout LayoutMail = new LinearLayout(mContext);
        //LayoutMail.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorSecondary, null));
        LayoutMail.setPadding(50,50,50,50);
        LayoutMail.setGravity(Gravity.CENTER | Gravity.CENTER);
        LayoutMail.setOrientation(LinearLayout.HORIZONTAL);
        layout.addView(LayoutMail,2);

        //SEPARATE
        layout.addView(ViewSeparate(),3);

        final ImageView IconMail = new ImageView(mContext);
        IconMail.setLayoutParams(new LinearLayout.LayoutParams(80, 80));
        IconMail.setBackgroundResource(R.drawable.ic_mail);
        IconMail.setColorFilter(Color.WHITE);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)IconMail.getLayoutParams();
        params.setMargins(0, 0, 50, 0);
        IconMail.setLayoutParams(params);
        LayoutMail.addView(IconMail);

        final TextView TextMail = new TextView(mContext);
        TextMail.setTextColor(Color.WHITE);
        TextMail.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1));
        TextMail.setTextSize(20);
        TextMail.setText(getString(R.string.ActivityDetailSupportMail));
        LayoutMail.addView(TextMail);

        final ImageView AddMail = new ImageView(mContext);
        AddMail.setLayoutParams(new LinearLayout.LayoutParams(50, 50));
        AddMail.setBackgroundResource(R.drawable.ic_add_plus);
        AddMail.setColorFilter(Color.RED);
        //AddMail.setColorFilter(ResourcesCompat.getColor(mContext.getResources(), R.color.colorSecondary, null));
        LayoutMail.addView(AddMail);

        //FORM
        final LinearLayout LayoutForm = new LinearLayout(mContext);
        LayoutForm.setVisibility(View.GONE);
        layout.addView(LayoutForm);


        LinearLayout Form = (LinearLayout) getLayoutInflater().inflate(R.layout.view_form, null);
        LayoutForm.addView(Form);

        TextMail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                SupportForm();
                /*
                if(LayoutMail.getVisibility() == View.VISIBLE){
                    LayoutMail.setVisibility(View.GONE);

                }else{
                    LayoutMail.setVisibility(View.VISIBLE);
                }
                */


            }
        });
    }

    private void SupportForm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, Style);
        builder.setTitle(getString(R.string.ActivityDetailSupportMailTitle));
        builder.setMessage(getString(R.string.ActivityDetailSupportMailMessage));

        //DETAIL
        LinearLayout BuilderLayout = new LinearLayout(mContext);
        BuilderLayout.setOrientation(LinearLayout.VERTICAL);
        BuilderLayout.setPadding(40,0,40,0);

        final EditText TextSubject = new EditText(mContext);
        TextSubject.setInputType(InputType.TYPE_CLASS_TEXT);
        TextSubject.setTextAppearance(mContext, R.style.TextInputLayout);
        TextSubject.setHintTextColor(Color.WHITE);
        TextSubject.setTextColor(Color.WHITE);
        TextSubject.setHint(mContext.getString(R.string.ActivityInputSubject));
        BuilderLayout.addView(TextSubject);

        final EditText TextContent = new EditText(mContext);
        TextContent.setTextAppearance(mContext, R.style.TextInputLayout);
        TextContent.setInputType(InputType.TYPE_CLASS_TEXT);
        TextContent.setHintTextColor(Color.WHITE);
        TextContent.setTextColor(Color.WHITE);
        TextContent.setSingleLine(false);
        TextContent.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
        TextContent.setHint(mContext.getString(R.string.ActivityInputTextArea));
        BuilderLayout.addView(TextContent);


        builder.setView(BuilderLayout);
        builder.setPositiveButton(getString(R.string.ActivityInputSend), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String StringSubject = TextSubject.getText().toString();
                String StringContent = TextContent.getText().toString();
                SendForm(StringSubject, StringContent);



            }
        });
        builder.setNegativeButton(getString(R.string.ActivityAutoButtonNegative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void SendForm(String Subject, String Content){
        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");
                    String title = response.getString("title");
                    new Class_General(mContext).CreateMenssage(title, message);

                    if(result.equals("success")){

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        try {
            //SUPPORT
            final JSONObject data = new JSONObject();
            data.put("action", "mail");
            data.put("option", "support");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("subject", Subject);
            data.put("content", Content);
            new Class_DataServer(mContext, data, response).execute("");

        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    private View ViewSeparate(){
        final TextView Separate = new TextView(mContext);
        Separate.setBackgroundColor(Color.BLACK);
        Separate.setTextSize(0);
        Separate.setPadding(0,3,0,0);
        return Separate;
    }


}
