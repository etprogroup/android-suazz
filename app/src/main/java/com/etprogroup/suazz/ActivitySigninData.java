package com.etprogroup.suazz;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivitySigninData extends ActivityRoot {
    private Context mContext;
    private Calendar SigninDate;
    private Button ButtonApply;
    private Class_User _User;

    private EditText userfname;
    private EditText userlname;
    private EditText userphone;
    private EditText userdate;
    private EditText usermail;
    private EditText userpass;

    private TextView TextPolicy;
    private ImageView IconPolicy;
    private LinearLayout LayoutPolicy;
    private Boolean CheckPolicy= false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signindata);
        mContext = this;


        //DATA
        _User = ((App)getApplicationContext()).USERData();
        userfname = (EditText) findViewById(R.id.EditName);
        userlname = (EditText) findViewById(R.id.EditLastName);
        userphone = (EditText) findViewById(R.id.EditPhone);
        userdate = (EditText) findViewById(R.id.EditDate);
        usermail = (EditText) findViewById(R.id.EditEmail);
        userpass = (EditText) findViewById(R.id.EditPass);

        //Button Data
        ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(CheckPolicy){
                    SignIn();
                }else{
                    ActivityPolicy();
                }
            }

        });

        //Policy
        TextPolicy = (TextView) findViewById(R.id.TextPolicy);
        IconPolicy = (ImageView) findViewById(R.id.IconPolicy);
        LayoutPolicy = (LinearLayout) findViewById(R.id.LayoutPolicy);
        LayoutPolicy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ActivityPolicy();
            }
        });
        CheckPolicy(false);


        userdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DatePickerDialog DatePicker = new DatePickerDialog(mContext, Style, CallBackDate,
                        SigninDate.get(Calendar.YEAR),
                        SigninDate.get(Calendar.MONTH),
                        SigninDate.get(Calendar.DAY_OF_MONTH));
                DatePicker.setTitle("Select Date");
                DatePicker.show();
            }

        });
    }



    final int Style = AlertDialog.THEME_DEVICE_DEFAULT_DARK; //THEME_HOLO_DARK //LIGHT
    final DatePickerDialog.OnDateSetListener CallBackDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            SigninDate.set(Calendar.YEAR, year);
            SigninDate.set(Calendar.MONTH, monthOfYear);
            SigninDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            GridDateUpdate(userdate);
        }

    };


    private void GridDateUpdate(TextView element) {
        String format = "MM/dd/yy";
        SimpleDateFormat formatted = new SimpleDateFormat(format, Locale.US);
        SetText(element, formatted.format(SigninDate.getTime()));
    }


    private void SetText(TextView element, String text) {
        element.setText(text);
    }


    public void SignIn(){
        JSONObject data = new JSONObject();
        try {
            data.put("action", "signin");
            data.put("fname", userfname.getText().toString());
            data.put("lname", userlname.getText().toString());
            data.put("phone", userphone.getText().toString());
            data.put("date", userdate.getText().toString());
            data.put("user", usermail.getText().toString());
            data.put("pass", userpass.getText().toString());
            data.put("passc", userpass.getText().toString());

        } catch (JSONException e){
            e.printStackTrace();
        }

        Class_DataServer.AsyncResponse response = new Class_DataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new Class_General(mContext).CreateMenssage("response", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");
                    final String title = response.getString("title");

                    if(result.equals("success")){
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        Intent Intent = new Intent(mContext, ActivitySigninConfirmed.class);
                        startActivityForResult(Intent, 0);

                    }else if(result.equals("error")){
                        new Class_General(mContext).CreateMenssage(title, message);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        new Class_DataServer(mContext, data, response).execute("");
    }


    private void ActivityPolicy(){
        Intent intent = new Intent(mContext, ActivityGeneralPolicy.class);
        startActivityForResult(intent, CAPTURE_POLICY);
    }

    private void CheckPolicy(Boolean check){
        CheckPolicy = check;
        if(CheckPolicy){
            IconPolicy.setImageResource(R.drawable.ic_mark_tick);
            IconPolicy.setColorFilter(Color.GREEN);
        }else{
            int color = ResourcesCompat.getColor(mContext.getResources(), R.color.colorSecondary, null);
            IconPolicy.setImageResource(R.drawable.ic_mark_cancel);
            IconPolicy.setColorFilter(color);

        }
    }


    private static final int CAPTURE_POLICY = 1;
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_POLICY) {
            if(resultCode == RESULT_OK) {
                CheckPolicy(true);
                //latitude = Double.parseDouble(data.getStringExtra("latitude"));
            }else{
                CheckPolicy(false);

            }
        }
    }

}
