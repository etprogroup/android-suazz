package com.etprogroup.suazz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.google.android.gms.internal.zzagz.runOnUiThread;

/**
 * Created by Bronco on 10-09-2017.
 */

public class Class_DataServerFile extends AsyncTask<String, Void, String> {
    public Activity mContext;
    public String upload = "";
    public String Authorization = "";
    public static final int CONNECTION_TIMEOUT = 15 * 1000;
    public final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;
    public ProgressDialog mProgressDialog = null;
    public AsyncResponse delegate = null;
    public JSONObject data;
    public String path = "";

    public Class_DataServerFile(Activity context, JSONObject JsonData, String path, AsyncResponse delegate) {
        this.mContext = context;
        this.delegate = delegate;
        this.data = JsonData;
        this.path = path;
        this.upload = mContext.getString(R.string.URLServer)+"data/upload.php";
        this.Authorization = mContext.getString(R.string.AppAuthorization);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(mContext != null && mContext instanceof Activity) {
            mProgressDialog = new ProgressDialog(mContext, R.style.CustomStyleProgressDialog);
            //mProgressDialog.setTitle("Conected");
            //mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }


    protected String doInBackground(String... params) {
        String JsonResponse = null;
        String JsonDATA = params[0];
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        File sourceFile = new File(path);
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        int serverResponseCode = 0;



        if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mContext,
                    new String[]{
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSIONS_WRITE_EXTERNAL_STORAGE);

            return null;

        }else{

            if (!sourceFile.isFile()) {
                Log.e("uploadFile", "Source File not exist : " + path);

            } else {

                try {
                    String StringURL = new Class_DataServer(mContext).Build_URLGet(upload, data);
                    String StringData = new Class_DataServer(mContext).getPostDataString(data);

                    //File
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);

                    //URL url = new URL(upload);
                    URL url = new URL(StringURL);

                    connection = (HttpURLConnection) url.openConnection();
                    connection.setReadTimeout(CONNECTION_TIMEOUT);
                    connection.setConnectTimeout(CONNECTION_TIMEOUT);
                    connection.setUseCaches(false);
                    connection.setDoOutput(true);
                    connection.setDoInput(true);

                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Authorization", Authorization);
                    connection.setRequestProperty("Connection", "Keep-Alive");
                    connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    connection.setRequestProperty("uploaded_file", path);

                    //connection.getOutputStream().write(data.toString().getBytes("UTF-8"));
                    DataOutputStream dos = new DataOutputStream(connection.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\""+data.getString("image_name")+"\";filename=\""+path+"\"" + lineEnd);
                    dos.writeBytes(lineEnd);

                    // create a buffer of  maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {

                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    }

                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    // Responses from the server (code and message)
                    serverResponseCode = connection.getResponseCode();
                    String serverResponseMessage = connection.getResponseMessage();

                    if (serverResponseCode == 200) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Log.i("uploadFile", "Path is : " + path);
                            }
                        });
                    }

                    //close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();
  /*

                    //connection.getOutputStream().write(StringData.getBytes("UTF-8"));
                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                    writer.write(StringData);
                    //writer.writeBytes("q="+StringData);
                    writer.flush();
                    writer.close();
                    os.close();
    */
                    connection.connect();


                    InputStream inputStream = connection.getInputStream();
                    if (inputStream == null) {
                        return null;
                    }

                    reader = new BufferedReader(new InputStreamReader(inputStream));
                    StringBuffer sbuffer = new StringBuffer();

                    String inputLine;
                    while ((inputLine = reader.readLine()) != null){
                        sbuffer.append(inputLine + "\n");
                    }

                    if (sbuffer.length() == 0) {
                        return null;
                    }

                    JsonResponse = sbuffer.toString();
                    return JsonResponse;

                } catch (IOException e) {
                    e.printStackTrace();

                } catch (Exception e) {
                    e.printStackTrace();

                } finally {
                    if (connection  != null) {
                        connection.disconnect();
                    }
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (final IOException e) {

                        }
                    }
                }
            }
        }
        return null;
    }

    protected void onPostExecute(String result) {
        //Log.d("processFinish","processFinish> "+result);
        if(result==null){
            result="";
        }
        delegate.processFinish(result);

        if(mProgressDialog != null){
            mProgressDialog.dismiss();

        }
    }

    public interface AsyncResponse {
        void processFinish(String output);
    }



    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new Class_DataServerFile(mContext, data, path, delegate).execute("");

                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
                return;
            }
        }
    }

}
