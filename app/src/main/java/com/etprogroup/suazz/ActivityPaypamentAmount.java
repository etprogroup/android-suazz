package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityPaypamentAmount extends ActivityRoot {
    public Activity mContext;
    private String booking;
    private String sbookings;
    private JSONObject bookings =  new JSONObject();
    private double ServiceTotal;
    private Class_Services _Services;
    private Class_User _User;

    private JSONObject Booking;
    private String token;
    private String title;
    private String provider;
    private String service;
    private String date;
    private String datetime;
    private String quantity;
    private String autos;
    private String price;
    private String duration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paypamentamount);
        mContext = this;

        _User = ((App)getApplicationContext()).USERData();
        _Services = ((App)getApplicationContext()).Services();

        //HEADER SETPS
        new Class_HeaderSteps(this, "payments");

        Intent data = getIntent();
        booking = data.getStringExtra("booking");
        sbookings = data.getStringExtra("bookings");

        if(booking!=null && sbookings!=null){
            if(!booking.equals("") && !sbookings.equals("")){
                try {
                    bookings = new JSONObject(sbookings);
                    FillData();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void FillData(){
        try {
            JSONObject Booking = bookings.getJSONObject(booking);
            token = Booking.getString("token");
            title = Booking.getString("title");
            provider = Booking.getString("provider");
            service = Booking.getString("service");
            date = Booking.getString("date");
            datetime = Booking.getString("datetime");
            quantity = Booking.getString("quantity");
            autos = Booking.getString("autos");
            price = Booking.getString("price");

            title = _Services.GET_Data(service, "title");
            duration = _Services.GET_Data(service, "duration");
            price = _Services.GET_Data(service, "price");

            double amount = Double.parseDouble(price);
            double autos = Double.parseDouble(quantity);
            double extra = 0.00;
            double total = (amount*autos)+extra;
            _User.SetDataServiceTotal(total);
            ServiceTotal = total;

            if(autos>2){
                TextView Discount = (TextView) findViewById(R.id.TextDiscount);
                Discount.setVisibility(View.VISIBLE);
                total =  (total * 0.6);
                _User.SetDataServiceTotal(total);
                ServiceTotal = total;
            }

            TextView Amount = (TextView) findViewById(R.id.TextPaypamentAmount);
            Amount.setText(title);

            TextView SubTotal = (TextView) findViewById(R.id.TextPaypamentAmountSubTotal);
            SubTotal.setText(mContext.getString(R.string.PaypalCurrencyIcon)+"  "+price);

            TextView Autos = (TextView) findViewById(R.id.TextPaypamentAmountAutos);
            Autos.setText(String.valueOf(quantity));

            LinearLayout LayoutExtra = (LinearLayout) findViewById(R.id.LayoutPaypamentAmountExtra);
            TextView Extra = (TextView) findViewById(R.id.TextPaypamentAmountExtra);
            Extra.setText(String.valueOf(mContext.getString(R.string.PaypalCurrencyIcon)+"  "+extra));
            if(extra == 0){
                LayoutExtra.setVisibility(View.GONE);
            }

            TextView Total = (TextView) findViewById(R.id.TextPaypamentAmountTotal);
            Total.setText(String.valueOf(mContext.getString(R.string.PaypalCurrencyIcon)+"  "+total));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //PAYPAL
        PayPalConfig();
    }






    //PAYPAL
    //Paypal Configuration Object
    private String PaypalTitle;
    private String PaypalCurrency;
    private String PaypalClientID;
    private Button buttonPaypament;
    private PayPalConfiguration config;

    //Paypal intent request code to track onActivityResult method
    public static final int PAYPAL_REQUEST_CODE = 123;

    private void PayPalConfig() {
        PaypalClientID = mContext.getString(R.string.PaypalClientID);
        PaypalCurrency = mContext.getString(R.string.PaypalCurrency);
        PaypalTitle = mContext.getString(R.string.PaypalTitle);

        config = new PayPalConfiguration()
                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
                .clientId(PaypalClientID);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);


        buttonPaypament = (Button) findViewById(R.id.ButtonApply);
        buttonPaypament.setEnabled(true);
        buttonPaypament.setVisibility(View.VISIBLE);
        buttonPaypament.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                getPayment();
            }
        });
    }

    @Override
    public void onDestroy() {
        //stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private void getPayment(){
        //Creating a PaypalPayment
        PayPalPayment payment = new PayPalPayment(
                new BigDecimal(String.valueOf(ServiceTotal)),
                PaypalCurrency,
                PaypalTitle,
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == PAYPAL_REQUEST_CODE) {
            Intent PayPalIntent = new Intent(this, ActivityPaypamentResult.class);
            PayPalIntent.putExtra("booking", booking);
            PayPalIntent.putExtra("bookings", sbookings);
            PayPalIntent.putExtra("PaymentTitle", PaypalTitle);
            PayPalIntent.putExtra("PaymentPrice", price);
            PayPalIntent.putExtra("PaymentAmount", String.valueOf(ServiceTotal));
            PayPalIntent.putExtra("PaypalCurrency", PaypalCurrency);
            PayPalIntent.putExtra("PaymentProvider", provider);
            PayPalIntent.putExtra("PaymentService", title);
            PayPalIntent.putExtra("PaymentAutosCount", quantity);
            PayPalIntent.putExtra("PaymentAutos", autos);
            PayPalIntent.putExtra("PaymentDateTimeStamp", datetime);
            PayPalIntent.putExtra("PaymentDate", date);
            PayPalIntent.putExtra("PaymentDetails", "");
            PayPalIntent.putExtra("PaymentMessage", "");
            PayPalIntent.putExtra("PaymentResult", "");
            PayPalIntent.putExtra("PaymentResultCode", "");

            if (resultCode == Activity.RESULT_OK) {
                PayPalIntent.putExtra("PaymentResultCode", "RESULT_OK");
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirm != null) {
                    try {
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        PayPalIntent.putExtra("PaymentDetails", paymentDetails);
                        PayPalIntent.putExtra("PaymentResult", "Success");
                        PayPalIntent.putExtra("PaymentMessage", "Transaction Success");
                        Log.i("paymentExample", paymentDetails);

                    } catch (JSONException e) {
                        PayPalIntent.putExtra("PaymentResult", "Error");
                        PayPalIntent.putExtra("PaymentMessage", "an extremely unlikely failure occurred:"+e);
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                PayPalIntent.putExtra("PaymentResultCode", "RESULT_CANCELED");
                PayPalIntent.putExtra("PaymentResult", "Canceled");
                PayPalIntent.putExtra("PaymentMessage", "The user canceled.");
                Log.i("paymentExample", "The user canceled.");

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                PayPalIntent.putExtra("PaymentResultCode", "RESULT_EXTRAS_INVALID");
                PayPalIntent.putExtra("PaymentResult", "Error de Configuracion");
                PayPalIntent.putExtra("PaymentMessage", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }

            startActivity(PayPalIntent);
        }
    }

}
