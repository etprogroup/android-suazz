package com.etprogroup.suazz;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

/**
 * Created by Bronco on 30-07-2017.
 */

public class App extends MultiDexApplication {
    public Application mContext;

    public Class_User _User;
    public Class_Autos _Autos;
    public Class_Locations _Locations;
    public Class_Services _Services;
    public Class_Providers _Providers;



    public App(){
        mContext = this;

        _User = new Class_User(mContext);
        _Autos = new Class_Autos(mContext);
        _Locations = new Class_Locations(mContext);
        _Services = new Class_Services(mContext);
        _Providers = new Class_Providers(mContext);

    }

    //USERDATA
    public Class_User USERData(){
        return _User;
    }

    //USERAUTOS
    public Class_Autos Autos(){
        return _Autos;
    }

    //USERMAPS
    public Class_Locations Locations(){
        return _Locations;
    }

    //SERVICES
    public Class_Services Services(){
        return _Services;
    }

    //PROVIDERS
    public Class_Providers Providers(){
        return _Providers;
    }

}
