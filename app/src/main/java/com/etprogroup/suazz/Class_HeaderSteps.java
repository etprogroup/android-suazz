package com.etprogroup.suazz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Bronco on 15-07-2017.
 */

public class Class_HeaderSteps extends AppCompatActivity {
    public Activity mContext;
    public String Step = "";

    public Class_HeaderSteps(Activity context) {
        this.mContext = context;

        CreateHeaderSteps();
        CreateToolbar();
    }

    public Class_HeaderSteps(Activity context, String Step) {
        this.mContext = context;
        this.Step = Step;

        CreateHeaderSteps();
        CreateToolbar();
    }

    public void CreateToolbar() {
        //TOOLBAR
        new Class_Toolbar(mContext);
    }

    public void CreateHeaderSteps() {

        LinearLayout LayoutService = (LinearLayout) mContext.findViewById(R.id.GridHeaderService);
        LayoutService.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RedirectSteps(ActivityServices.class);
            }
        });


        LinearLayout LayoutAutos = (LinearLayout) mContext.findViewById(R.id.GridHeaderAutos);
        LayoutAutos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RedirectSteps(ActivityAutos.class);
            }
        });

        LinearLayout LayoutLocation = (LinearLayout) mContext.findViewById(R.id.GridHeaderLocation);
        LayoutLocation.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RedirectSteps(ActivityMaps.class);
            }
        });

        LinearLayout LayoutPayment = (LinearLayout) mContext.findViewById(R.id.GridHeaderPayment);
        LayoutPayment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RedirectSteps(ActivityProfileBookingPayment.class);
            }
        });

        if(!Step.equals("")){
            StepsSelect(Step);
        }
    }

    public void RedirectSteps(Class activity){
        Intent intent = new Intent(mContext, activity);
        mContext.startActivity(intent);
    }

    public int ColorSteps(int color){
        return ResourcesCompat.getColor(mContext.getResources(), color, null);
    }


    public void StepsSelect(String steps){
        TextView Title = (TextView) mContext.findViewById(R.id.TitleGridHeaderService);
        ImageView Icon = (ImageView) mContext.findViewById(R.id.IconGridHeaderService);

        if(steps.equals("services")){

        }else if(steps.equals("autos")){
            Title = (TextView) mContext.findViewById(R.id.TitleGridHeaderAutos);
            Icon = (ImageView) mContext.findViewById(R.id.IconGridHeaderAutos);

        }else if(steps.equals("locations")){
            Title = (TextView) mContext.findViewById(R.id.TitleGridHeaderLocation);
            Icon = (ImageView) mContext.findViewById(R.id.IconGridHeaderLocation);

        }else if(steps.equals("payments")){
            Title = (TextView) mContext.findViewById(R.id.TitleGridHeaderPayment);
            Icon = (ImageView) mContext.findViewById(R.id.IconGridHeaderPayment);

        }

        Title.setTextColor(ColorSteps(R.color.colorSecondary));
        Icon.setColorFilter(ColorSteps(R.color.colorSecondary));
    }
}
